export const findByTestId = (wrapper, val) => {
	return wrapper.find(`[test-id="${val}"]`)
}
