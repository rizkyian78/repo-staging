import axios from 'axios'
import { BASE_URL } from '../constants'
axios.defaults.baseURL = `${BASE_URL}/v1`;
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers['x-access-token'] = localStorage.getItem('token');
axios.defaults.headers.common['Content-Type'] = 'multipart/form-data; boundary="another cool boundary"';

function uploadFileZip(fData) {
	return axios.post(`products-upload-zip`, fData)
}

const apiservices = {
	uploadFileZip
}

export default apiservices