import jwt from 'jsonwebtoken'

export function canMenuVendor() {
	const token = localStorage.getItem('token')

	let role = token && jwt.decode(token).user.role

	return role === 1
}

export function canCreateOrder() {
	const token = localStorage.getItem('token')

	let role = token && jwt.decode(token).user.role

	return role !== 1
	// return role === 3 || role === 5
}

export function belongs(item, field) {
	const token = localStorage.getItem('token')

	let value = token && jwt.decode(token).user[field]

	return item === value
}

const authorize = {
	canMenuVendor,
	canCreateOrder,
	belongs
}

export default authorize
