import axios from 'axios'
import jwt from 'jsonwebtoken'

import { BASE_URL } from '../constants'

function login(data){
	return axios.post(`${BASE_URL}/v1/login`, data)
} 

function logout(){
	localStorage.removeItem('token')
	localStorage.removeItem('_idverify')
	window.location = '/'
}

function isAuth() {
	const token = localStorage.getItem('token')

	let user = token && jwt.decode(token).user

	if (user) {
		return true
	} else {
		return false
	}
}

function getUser() {
	const token = localStorage.getItem('token')

	let user = token && jwt.decode(token).user

	return user
}

function getRole() {
	const token = localStorage.getItem('token')

	let role = token && jwt.decode(token).user.role

	return role
}

const auth = {
	login,
	logout,
	isAuth,
	getUser,
	getRole
}

export default auth
