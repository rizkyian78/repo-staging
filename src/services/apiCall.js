import axios from 'axios'
import { BASE_URL } from '../constants'

export default axios.create({
	baseURL: `${BASE_URL}/v1`,
	headers: {
		'x-access-token': localStorage.getItem('token')
	}
})
