import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import Slider from 'react-slick'
import swal from 'sweetalert2'
import apiCall from '../../services/apiCall'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { BASE_URL } from '../../constants'

import SearchBar from '../../components/SearchBar/SearchBar'

class Home extends Component {

	constructor(props) {
		super(props)
		this.state = {
			vendors: []
		}
	}

	componentDidMount() {
		this.fetchVendors()
	}

	fetchVendors = () => {
		apiCall.get(`/vendors`)
		.then(res => {
			this.setState({
				vendors: res.data.data,
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.message, '', 'error')
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	render() {
		let settings = {
		  dots: true,
		  infinite: true,
		  speed: 500,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 3000,
		  pauseOnHover: true,
		}

		return (
			<div>
				<img src="/img/bg.jpg" style={{height: 400, width: '100%', objectFit: 'cover', position: 'absolute'}} alt=""/>
				<section className="section">
					<div className="container">
						<div className="columns">
							<div className="column is-6 is-offset-3" style={{background: '#ffffff38', borderRadius: 20}}>
								<div className="has-text-centered">
									<br/>
								  <img src="/img/logo.png" width="100" height="28" alt="logo" />
									<br/>
								  <h5 className="title is-5">Direct Buying</h5>
									<br/>
								</div>
								<SearchBar
									size="medium"
									history={this.props.history}
								/>

								<br/>
								<br/>
								<div className="has-text-centered">	
									<Link to="/search" className="button is-light">
										Semua Produk
									</Link>
								</div>
							</div>
						</div>

						<hr/>
						
						<br/>
						<h4 className="title is-4 has-text-centered">Vendor</h4>
						<br/>

						<Slider {...settings}>
						  {
						    this.state.vendors.map((item, index) => (
						    	<div className="slide-item" key={index}>
						    		<div className="columns" >
						    			<div className="column is-6">
						    				<img
						    				  src={`${BASE_URL}/vendors/${item.images}`}
						    				  alt="Images Vendor" 
						    				  style={{height: 400, width: '100%', objectFit: 'cover'}}
						    				/>
						    			</div>
						    			<div className="column">
						    				<h5 className="title is-4">{item.name}</h5>
						    				<p>
						    					<b>Bidang Usaha</b>: {item.business}
						    					<br/>
						    					<b>Alamat</b>: {item.address}
						    					<br/>
						    					<b>Contact Person</b>: {item.contactPerson}
						    					<br/>
						    					<b>HP</b>: {item.phone}
						    					<br/>
						    					<b>Keterangan</b>: {item.position}
						    				</p>
						    			</div>
						    		</div>
						    	</div>
						    ))
						  }
						</Slider>

						<div className="columns">
							<div className="column has-text-centered">
								<hr/>

								<h4 className="title is-4">Panduan Flow Aplikasi</h4>
								<img src="/img/flow.png" alt="flow"/>
								<span>versi 1.1.9</span>
							</div>
						</div>
					</div>
				</section>
				
				<hr/>
				<h4 className="title is-4 has-text-centered">Pertamina RU IV Cilacap</h4>
				<img src="/img/all.jpg" style={{height: 400, width: '100%', objectFit: 'cover', position: 'absolute'}} alt=""/>
			</div>
		);
	}
}

export default Home
