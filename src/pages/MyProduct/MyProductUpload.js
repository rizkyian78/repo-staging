import React, { Component } from 'react';
import swal from 'sweetalert2'
import { Link } from 'react-router-dom'
import readXlsxFile from 'read-excel-file'

import { BASE_URL } from '../../constants'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'

class MyProductUpload extends Component {
	state = {
		products: [],
		loading: false
	}

	handleFileChange = (e) => {
		const input = document.getElementById("files")
    if (input.value !== '') {
      this.setState({loadImport: true})
    } else {
      this.setState({loadImport: false})
    }
    readXlsxFile(input.files[0]).then((rows) => {
      rows.shift()
      const dataJson = []
      rows.forEach((doc) => {
        const row = {
        	vendor: auth.getUser()._id,
          name: doc[1],
          specification: doc[2],
          brand: doc[3],
          price: doc[4],
          featuredImage: doc[5],
          images: doc[6] === null ? null : doc[6].split(';') 
        }
        dataJson.push(row)
      })
      console.log(dataJson, 'dataJson')
      this.setState({products: dataJson})
    })
	}

	handleSubmit = () => {
		this.setState({loading: true});

		apiCall.post('/products-upload-excel', {
			formData: this.state.products
		})
		.then(res => {
			this.setState({loading: false, products: []});
			swal.fire('Sukses Upload Data', '', 'success')
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	handleImageChange = (e) => {
		if (this.state.products.length === 0) {
			return swal.fire('Upload produk dulu!', '', 'info')
		}

		if (this.state.products.length > 1000) {
			return swal.fire('Max upload 1000 record/data', '', 'info')
		}

		swal.fire({
			title: "Loading ...",
			text: "",
			allowOutsideClick: false
		})
		swal.showLoading()

		let files = e.target.files
		let imageFiles = []

		for(let file of files) {
			let reader = new FileReader()
			reader.onloadend = (ev) => {	
				imageFiles.push(ev.target.result)
			}

			reader.readAsDataURL(file)
		}

		apiCall.post('/products-upload-image', {
			imageFiles
		})
		.then(res => {
			swal.fire('Sukses Upload Data', '', 'success')
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	render() {
		const { products, loading } = this.state
		return (
			<div>
				<div className="box">
					<div className="field">
						<label className="label">
							Upload Produk (Excel)
							&nbsp;
							<a 
								href={`${BASE_URL}/files/Format_Import_Product.xlsx`} 
								className="button is-primary is-small is-outlined is-rounded is-pulled-right"
							>
								<span className="icon">
									<i className="fa fa-file-excel"></i>
								</span>
								&nbsp;
								Contoh File
							</a>
						</label>
						<div className="control">
							<div className="file">
							  <label className="file-label">
							    <input 
							    	className="file-input" 
							    	type="file" 
							    	name="file" 
							    	id="files"
							    	accept=".xlsx, .xls, .csv" 
							    	onChange={this.handleFileChange}
							    />
							    <span className="file-cta">
							      <span className="file-icon">
							        <i className="fas fa-upload"></i>
							      </span>
							      <span className="file-label">
							        Upload File...
							      </span>
							    </span>
							  </label>
							</div>
						</div>
					</div>
					<br/>
					*Keterangan:
					<ol style={{marginLeft: 20}}>
						<li>Max Upload 1000 record/data</li>
						<li>Masukkan nama gambar di coloum <b>Cover Picture</b> untuk gambar yang di jadikan cover (hanya 1 gambar)</li>
						<li>Masukkan Nama gambar di coloum <b>Picture</b> dengan pemisah <b>; (titik koma)</b> jika gambar lebih dari satu</li>
						<li>Format nama gambar <b>YYYYMMDD[no urut gambar(5 digit)]</b>, exp. 19070800001.png</li>
					</ol>
					<br/>
					<br/>
					{
						products.length > 0 &&
						<div>
							<h6 className="title is-6">Data dari file excel:</h6>
							<p>{products.length} Produk</p>

							<div className="field is-grouped is-pulled-right">
								<div className="control">
								  <Link to="/my-products" className="button is-text">Cancel</Link>
								</div>
							  <div className="control">
							    <button className={`button is-link is-rounded ${loading && 'is-loading'}`} onClick={this.handleSubmit}>Simpan</button>
							  </div>
							</div>
						</div>
					}
					<br/>
					<br/>
					<hr/>
				</div>
				
			</div>
		);
	}
}

export default MyProductUpload
