import React, { Component } from 'react';
import swal from 'sweetalert2'
import { Link } from 'react-router-dom'

import { BASE_URL } from '../../constants'
import apiCall from '../../services/apiCall'
import Loading from '../../components/Loading'

class ProductUploadImages extends Component {
	state = {
		products: [],
		loading: false,
		files: null,
		load: false,
		file: null
	}

	handleFileChange = (e) => {
		const input = document.getElementById("files")
		const file = input.files[0]
		let reader = new FileReader()
		reader.loadstart = () => {
			this.setState({load: true})
		}
		reader.onloadend = (ev) => {	
			this.setState({ 
				files: ev.target.result,
				file: file,
				load: false
    	})
		}
		reader.readAsDataURL(file)
	}

	handleSubmit = () => {
		this.setState({loading: true});
		const {files} = this.state
		apiCall.post('/products-upload-zip', {files})
		.then(res => {
			this.setState({loading: false, files: null, file: null});
			swal.fire('Sukses Upload Data', '', 'success')
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	render() {
		const { file, loading } = this.state
		return (
			<div>
				<div className="box">
					<div className="field">
						<label className="label">
							Upload Gambar Produk (.zip)
							&nbsp;
							<a 
								href={`${BASE_URL}/files/FilesImages.zip`} 
								className="button is-primary is-small is-outlined is-rounded is-pulled-right"
							>
								<span className="icon">
									<i className="fa fa-file-excel"></i>
								</span>
								&nbsp;
								Contoh File
							</a>
						</label>
						<div className="control">
							<div className="file">
							  <label className="file-label">
							    <input 
							    	className="file-input" 
							    	type="file" 
							    	name="file" 
							    	id="files"
							    	accept=".zip" 
							    	onChange={this.handleFileChange}
							    />
							    <span className="file-cta">
							      <span className="file-icon">
							        <i className="fas fa-upload"></i>
							      </span>
							      <span className="file-label">
							        Upload File...
							      </span>
							    </span>
							  </label>
							</div>
						</div>
					</div>
					<br/>
					*Keterangan:<br />
					Pastikan nama gambar sesuai dengan nama gambar yang di upload di file excel
					<br/>
					<br/>
					{
						file !== null &&
						<div>
							<h6 className="title is-6">Data dari file excel:</h6>
							<p>{file.name}</p>

							<div className="field is-grouped is-pulled-right">
								<div className="control">
								  <Link to="/my-products" className="button is-text">Cancel</Link>
								</div>
							  <div className="control">
							    <button className={`button is-link is-rounded ${loading && 'is-loading'}`} onClick={this.handleSubmit}>Simpan</button>
							  </div>
							</div>
						</div>
					}
					<br/>
					<br/>
					<hr/>
				</div>
				<Loading load={this.state.load} />
			</div>
		);
	}
}

export default ProductUploadImages
