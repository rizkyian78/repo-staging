import React, { Component } from 'react';
import swal from 'sweetalert2'

import MyProductForm from './MyProductForm'

import apiCall from '../../services/apiCall'
import authorize from '../../services/authorize'

class MyProductEdit extends Component {
	state = {
		data: {}
	}

	componentDidMount() {
		const id = this.props.location.pathname.split('/')[2]

		apiCall.get(`/products/${id}`)
		.then(res => {
			const data = res.data.data
			if (!authorize.belongs(data.vendor && data.vendor.companyName, 'companyName')) {
				this.props.history.push('/login')
			}
			else {
				this.setState({data})
			}
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	render() {
		return (
			<section className="section">
				<div className="container">
					<h5 className="title is-5 has-text-centered">Edit Produk</h5>
					<div className="columns">
						<div className="column is-6 is-offset-3">
							<MyProductForm
								edit
								history={this.props.history}
								formData={this.state.data}
							/>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default MyProductEdit