import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import MyProductForm from './MyProductForm'
import MyProductUpload from './MyProductUpload'
import ProductUploadImages from './ProductUploadImages'

class MyProductCreate extends Component {
	state = {
		tabActive: 1
	}

	render() {
		const { tabActive } = this.state

		return (
			<section className="section">
				<div className="container">
					<h5 className="title is-5 has-text-centered">Tambahkan Produk</h5>
					<div className="columns">
						<div className="column is-6 is-offset-3">
							<div className="tabs is-small">
								<ul>
									<li className={tabActive === 1 ? 'is-active' : ''}>
										<Link onClick={() => this.setState({tabActive: 1})} to="#">Satuan</Link>
									</li>
									<li className={tabActive === 2 ? 'is-active' : ''}>
										<Link onClick={() => this.setState({tabActive: 2})} to="#">Banyak</Link>
									</li>
									<li className={tabActive === 3 ? 'is-active' : ''}>
										<Link onClick={() => this.setState({tabActive: 3})} to="#">Upload Gambar</Link>
									</li>
								</ul>
							</div>
							
							{
								tabActive === 1 &&
									<MyProductForm />
							}
							{
								tabActive === 2 &&
									<MyProductUpload />
							}
							{
								tabActive === 3 &&
									<ProductUploadImages />
							}
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default MyProductCreate