import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import Pagination from 'rc-pagination'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'
import authorize from '../../services/authorize'
import queryString from '../../utils/queryString'
import Loading from '../../components/Loading'

import ProductList from '../../components/ProductList/ProductList'

class MyProduct extends Component {
	state = {
		data: [],
		keyword: '',
		page: 1,
		total: 0,
		activePage: 1,
		load: false
	}

	componentDidMount() {
		if (!auth.isAuth()) 
			this.props.history.push('/login')
		else
    	this.updateSearch(this.props)
	}

	componentWillReceiveProps(nextProps) {
    this.updateSearch(nextProps)
	}

	updateSearch = (props) => {
		let params = queryString.parse(props.location.search)
		const idVendor = localStorage.getItem('_idverify')
		const page = params.page

		this.setState({
			idVendor,
			page: Number(page) || 1
		});
		this.fetchProducts(idVendor, page)
	}

	fetchProducts = async (idVendor, page) => {
		this.setState({load: true})
		apiCall.get('/my-products-vendor', {
			params: { 
				idVendor: idVendor,
				page,
				type: 'self'
			},
		})
		.then((response) => {
			this.setState({
				data: response.data.data,
				total: response.data.total,
				load: false
			});
		})
		.catch((err) => {
			this.setState({load: false})
		})
	}

	handlePageChange = (page) => {
    this.setState({activePage: page})
    this.props.history.push(`/my-products?page=${page}`)
  }

	renderMenuVendor = () => {
		return (
			<Link to="/my-products/create" className="button is-small is-primary is-outlined is-rounded tooltip" data-tooltip="Tambahkan Produk">
				<span className="icon">
				  <i className="fas fa-plus"></i>
				</span>
				&nbsp;
			 	Produk
			</Link>
		)
	}

	render() {
		return (
			<section className="section">
				<div className="container">
					<div className="columns">
						<div className="column is-8 is-offset-2">
							<div className="columns is-mobile">
								<div className="column">
									<h5 className="title is-5">
										My Product
									</h5>
								</div>
								<div className="column">
									<div className="is-pulled-right">
										{
											authorize.canMenuVendor() && 
											this.renderMenuVendor()
										}
									</div>
								</div>
							</div>

							<ProductList 
								data={this.state.data}
								total={this.state.total}
								keyword={this.state.keyword}
							/>
						</div>
						<div className="column">

						</div>
					</div>
					<hr />
					<div className="columns">
						<div className="column has-text-centered">
							{
                this.state.total > 0 ?
	              <Pagination 
	                current={this.state.activePage}
	                total={this.state.total}
	                defaultCurrent={this.state.activePage}
	                pageSize={8}
	                onChange={this.handlePageChange}
	                showLessItems={true}
	                 />
	              :
	              <div className="columns">
									<div className="column">
										<i className="fas fa-shopping-cart" style={{width: 250, height: 250}}></i><br />
										<label style={{fontSize: 30}}>Anda Tidak Memiliki Produk</label>
									</div>
								</div>
              }
						</div>
					</div>
				</div>
				<Loading load={this.state.load} />
			</section>
		);
	}
}

export default MyProduct
