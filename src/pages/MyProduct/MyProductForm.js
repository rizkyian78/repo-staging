import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import NumberFormat from 'react-number-format';
import swal from 'sweetalert2'
import moment from 'moment'

import apiCall from '../../services/apiCall'
import { BASE_URL } from '../../constants'

const initialState = {
	formData: {
		specs: [{}],
		images: [null, null, null, null]
	},

	loading: false,
	imagePreviewUrl: [null, null, null, null],
	imageFiles: [null, null, null, null],
}

class MyProductForm extends Component {
	state = {
		formData: {
			specs: [{}],
			images: [null, null, null, null]
		},

		loading: false,
		imagePreviewUrl: [null, null, null, null],
		imageFiles: [null, null, null, null],
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.edit) {
			this.setState({
				formData: nextProps.formData
			});
		}
	}

	handleInputChange = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;

		let { formData } = this.state

		formData[name] = value
		this.setState({formData})
	}

	addSpecs = () => {
		let { formData } = this.state

		formData['specs'].push({})
		this.setState({formData})
	}

	removeSpecs = (index) => {
		let { formData } = this.state

		formData['specs'].splice(index, 1)
		this.setState({formData})
	}

	handlelabelSpecsChange = (e, index) => {
		let { formData } = this.state

		formData['specs'][index] = {[e.target.value]: ''}
		this.setState({formData})
	}

	handleSpecsChange = (e, index) => {
		let { formData } = this.state

		let key = Object.keys(formData['specs'][index])[0]
		formData['specs'][index] = {[key]: e.target.value}

		this.setState({formData})
	}

	handleImageChange = (e, index) => {
		const { formData, imagePreviewUrl, imageFiles } = this.state
		let reader = new FileReader()
		let file = e.target.files[0]

		reader.onloadend = (ev) => {	
			let fileName = (formData.name ? formData.name.substring(0,25) : moment()) + '_' + index +'.jpg'
			fileName = fileName.replace(/[ ]/g, "_")

			formData['images'][index] = fileName
			imageFiles[index] = ev.target.result
			imagePreviewUrl[index] = reader.result

			this.setState({ 
				formData,
				imageFiles,
	      imagePreviewUrl
	    })
		}

		reader.readAsDataURL(file)
	}

	renderSpecs = (item, index) => {
		const { formData } = this.state

		let key = Object.keys(formData['specs'][index])[0]

		return(
			<div className="columns is-mobile" key={index}>
				<div className="column is-4">
					<div className="control">
					  <input className="input" type="text" value={key ? key : ''} onChange={(e) => this.handlelabelSpecsChange(e, index)} />
					</div>
				</div>
				<div className="column is-7">
					<div className="control">
						<textarea 
							className="textarea" 
							onChange={(e) => this.handleSpecsChange(e, index)}
							value={formData['specs'][index][key] ? formData['specs'][index][key] : ''}
							rows="1"
						>
						</textarea>
					</div>
				</div>
				<div className="column is-1">
					<button 
						className="button is-rounded tooltip"
						data-tooltip="Hapus" 
						onClick={() => this.removeSpecs(index)}
					>
						X
					</button>
				</div>
			</div>
		)
	}

	renderImage = (item, index) => {
		const { imagePreviewUrl } = this.state
		let view

		if(imagePreviewUrl[index] !== null) {
			view = <img alt="default" src={`${imagePreviewUrl[index]}`} style={{objectFit: 'contain'}} />
		}	else {
			if (item) {
				view = <img alt="default" src={`${BASE_URL}/img/${item}`} style={{objectFit: 'contain'}} />
			} else {
				view = <img alt="default" src="/img/add_product.png" style={{objectFit: 'cover'}} />
			}
		}
			
		return (
			<div className="column is-3" key={index}>
				<label className="hoverCursor">
					<figure className="image is-square">	
						{view}
					</figure>
					
					<input 
						type="file" 
						style={{display:'none'}} 
						onChange={(e) => this.handleImageChange(e, index)}
						accept="image/*"
					/>
				</label>
			</div>
		)
	}

	handleSubmit = () => {
		this.setState({loading: true});
		const { formData, imageFiles } = this.state

		if (typeof formData['price'] === 'string') {
			let numberPrice = formData.price.replace(/[Rp .]/g, "")
			formData['price'] = Number(numberPrice)
		}
		formData['featuredImage'] = formData['images'][0]
		if (this.props.edit) {
			this.update(formData, imageFiles)
		} else {
			this.post(formData, imageFiles)
		}
	}

	post = (formData, imageFiles) => {
		apiCall.post('/products', {
			formData,
			imageFiles
		})
		.then(res => {
			this.setState({loading: false});
			swal.fire('Sukses menambahkan produk', '', 'success')
			this.setState(initialState)
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			console.log(err.message, 'error insert produk')
			this.setState({loading: false});
		})
	}

	update = (formData, imageFiles) => {
		apiCall.put(`/products/${formData._id}`, {
			formData,
			imageFiles
		})
		.then(res => {
			this.setState({loading: false});
			swal.fire('Sukses edit produk', '', 'success')
			.then(() => {
				window.location = `/products/${formData._id}`
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	render() {
		const { formData, loading } = this.state

		return (
			<div className="box">
				<div className="field">
					<label className="label">Gambar Produk</label>
					<div className="control">
						<div className="columns is-mobile">
							{
								formData.images.map((item, index) => (
									this.renderImage(item, index)
								))
							}
						</div>
					</div>
				</div>

				<br/>

				<div className="field">
				  <label className="label">Nama Produk (Short Description)</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="name" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.name ? formData.name : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Brand / Manufacture</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="brand" 
				    	type="text" 
				    	onChange={this.handleInputChange}
				    	value={formData.brand ? formData.brand : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Harga</label>
				  <div className="control">
				  	<NumberFormat
				  		required 
				  		value={formData.price ? formData.price : ''}
				  		thousandSeparator={'.'}
				  		decimalSeparator={','}
				  		prefix={'Rp '} 
				  		className="input"
				  		name='price'
				  		onChange={this.handleInputChange}
				  	/>
				  </div>
				</div>

				<div className="field">
				  <label className="label">Full Specification</label>
				  <div className="control">
				    <textarea 
				    	className="textarea"
				    	rows="10" 
				    	name="specification" 
				    	onChange={this.handleInputChange}
				    	value={formData.specification ? formData.specification : ''}>
				    </textarea>
				  </div>
				</div>

				<hr/>

				<div className="field is-grouped is-pulled-right">
					<div className="control">
					  <Link to="/my-products" className="button is-text">Cancel</Link>
					</div>
				  <div className="control">
				    <button className={`button is-link is-rounded ${loading && 'is-loading'}`} onClick={this.handleSubmit}>Simpan</button>
				  </div>
				</div>
				<br/>
				<br/>
			</div>
		);
	}
}

export default MyProductForm
