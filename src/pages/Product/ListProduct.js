import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'

class ListProduct extends Component {
	state = {
		labelActive:0,
		products: [],
		load: true
	}

	componentDidMount() {
		if (auth.getRole() !== 9) {
			this.props.history.push('/login')
		}
	}

	fetchProducts = async (state, instance) => {
		const { page, pageSize, filtered, sorted } = state ? state : this.state;
    const filterString = JSON.stringify(filtered);
    const sort = JSON.stringify(sorted);
    let params = `?page=${page}&pageSize=${pageSize}&sorted=${sort}&filtered=${filterString}`
    const paramsEncode = encodeURI(params)
		apiCall.get(`/all-products${paramsEncode}`)
		.then((res) => {
			this.setState({
				products: res.data.data,
				load: false,
				currentPage: page,
        perPage: pageSize,
        pages: parseInt(res.data.totalRow / pageSize, 10) + (res.data.totalRow % pageSize > 0 ? 1 : 0),
        page, pageSize, filtered, sorted
			});
		})
		.catch((err) => {
			this.setState({load: false})
			console.log(err)
		})
	}

	renderProduct = () => {		
		const columns = [{
	    Header: 'No',
	    filterable: false,
	    minWidth: 75,
	    Cell: props => <span>{props.index + 1}</span>
	  }, {
	    Header: 'Nama',
	    accessor: 'name'
	  }, {
	    Header: 'Brand',
	    accessor: 'brand'
	  }, {
	    Header: 'Code',
	    accessor: 'code'
	  }, {
	    Header: 'Kode Vendor',
	    accessor: 'vendor'
	  }, {
	    Header: 'Aksi',
	    filterable: false,
	    Cell: props => (
           <div>
           <center>
               <Link 
									to={`/update-product-vendor/${props.original._id}`}
									className="button is-outlined is-small is-rounded"
								>
									<span className="icon">
									  <i className="fas fa-pencil-alt"></i>
									</span>
									&nbsp;
									Edit
								</Link>
            </center>
           </div>
       )
	  }]
		return (
				<ReactTable
					manual
			    data={this.state.products}
			    columns={columns}
			    filterable
          defaultPageSize={10}
          pages={this.state.pages}
			    loading={this.state.load}
          className="-highlight"
          onFetchData={this.fetchProducts}
			  />
			);
	}

	render() {
		return (
			<section className="section">
				<div className="container">
					<div className="columns is-mobile">
						<div className="column">
							<div className="is-pulled-right">
								List Product
							</div>
						</div>
					</div>
					<div className="table-container">
						{this.renderProduct()}
					</div>
				</div>
			</section>
		);
	}
}

export default ListProduct
