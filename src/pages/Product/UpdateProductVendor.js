import React, { Component } from 'react';
import swal from 'sweetalert2'

import apiCall from '../../services/apiCall'

class UpdateProductVendor extends Component {
	state = {
		data: {},
		userVendors: [],
		name: '',
		vendor: '',
		loading: false,
		_id: ''
	}

	componentDidMount() {
		this.fetchVendors()
		const id = this.props.location.pathname.split('/')[2]
		apiCall.get(`/products/${id}`)
		.then(res => {
			const data = res.data.data
			console.log(data)
			this.setState({
				name: data.name,
				_id: id,
				vendor: data.vendor ? data.vendor._id : '' })
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	fetchVendors = () => {
		apiCall.get(`/users/vendors`)
		.then(res => {
			this.setState({
				userVendors: res.data.data,
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.message, '', 'error')
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	handleSubmit = (e) => {
		e.preventDefault()
		this.setState({loading: true});
		const {_id, vendor} = this.state
		apiCall.post('/update-product', {
			id: _id,
			vendor: vendor
		})
		.then(res => {
			this.setState({loading: false});
			swal.fire('Sukses update produk', '', 'success')
			window.location = '/list-product'
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	handleInput = (e) => {
		console.log(e.target.value, '===')
		this.setState({[e.target.name]: e.target.value})
	}

	render() {
		const {userVendors, loading} = this.state
		return (
			<section className="section">
				<div className="container">
					<h5 className="title is-5 has-text-centered">Edit Product</h5>
					<div className="columns">
						<div className="column is-6 is-offset-3">
							<form>
								<div className="field">
								  <label className="label">Nama Vendor</label>
								  <div className="control">
								    <input 
								    	className="input" 
								    	name="name" 
								    	type="text" 
								    	value={this.state.name}
								    	readOnly
								    />
								  </div>
								</div>
			    			<div className="field">
								  <label className="label">Nama Vendor</label>
								  <div className="control">
								    <select 
								    	className="input"
								    	name="vendor" 
								    	onChange={this.handleInput} 
								    	value={this.state.vendor}
								    	required
								    >
								    	<option value="">- Pilih Perusahaan -</option>
								    	{
								    		userVendors.map((item, index) => (
								    				<option key={index.toString()} value={`${item._id}`}>{item.vendor ? item.vendor.name : item.name}</option>		
								    			)
								    		)
								    	}
								    </select>
								  </div>
								</div>
								<div className="control">
							    <button type="submit" className={`button is-link is-rounded ${loading && 'is-loading'}`} onClick={this.handleSubmit}>Update</button>
							  </div>
							</form>
					</div>
				</div>
				</div>
			</section>
		);
	}
}

export default UpdateProductVendor
