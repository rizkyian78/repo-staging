import React, { Component } from 'react';
import ReactTable from 'react-table'
import swal from 'sweetalert2'
import numeral from 'numeral'
import { Link } from 'react-router-dom'
import 'rc-pagination/assets/index.css'
import { BASE_URL } from '../../constants'
import authorize from '../../services/authorize'
import auth from '../../services/auth'
import apiCall from '../../services/apiCall'

class PicProduct extends Component {
	state = {
		products: [],
		pages: null,
		currentPage: 1,
		perPage: 10,
		loading: true,
		typeProd: null,
		company: '',
		verify: '',
		vendors: [],
		page: null,
		pageSize: null,
		filtered: null,
		sorted: null,
		edit: false,
		code: '',
		stockType: 'stock',
		textVerif: '',
		itemsOne: {},
		selectedImage: null,
	}

	componentDidMount() {
		if (auth.getRole() !== 2) {
			this.props.history.push('/')
		}
    this.fetchVendors()
	}

	selectImage = (image) => {
		this.setState({selectedImage: image});
	}

	fetchProducts = async (state, instance) => {
		const { page, pageSize, filtered, sorted } = state ? state : this.state;
		const {company, verify} = this.state
    const filterString = JSON.stringify(filtered);
    const sort = JSON.stringify(sorted);
    let params = `?page=${page}&pageSize=${pageSize}&sorted=${sort}&filtered=${filterString}&company=${company}&verify=${verify}`
    const paramsEncode = encodeURI(params)
		apiCall.get(`/products-by-pic${paramsEncode}`)
		.then(res => {
			console.log(res)
			this.setState({
				products: res.data.data,
				currentPage: page,
        perPage: pageSize,
        pages: parseInt(res.data.totalRow / pageSize, 10) + (res.data.totalRow % pageSize > 0 ? 1 : 0),
        loading: false,
        page, pageSize, filtered, sorted
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	fetchVendors = () => {
		apiCall.get(`/vendors`)
		.then(res => {
			this.setState({
				vendors: res.data.data,
			})
		})
		.catch(err => {
			console.log('')
		})
	}


  handleInput = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}

	handleInputChange = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;
		this.setState({
			[name]: value,
			page: 0,
			pageSize: 10
		}, () => {
			this.fetchProducts()
		})
	}

	handleInputChangeVerify = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;
		this.setState({
			[name]: value,
			page: 0,
			pageSize: 10
		}, () => {
			this.fetchProducts()
		})	
	}

	handleSubmit = () => {
		const { id } = this.state
		const fData = {
			code: this.state.code,
			stockType: this.state.stockType
		}
		apiCall.put(`/products/verifikasi/${id}`, {
			formData: fData
		})
		.then(res => {
			swal.fire('Sukses Update Data produk', '', 'success')
			.then(() => {
				this.setState({edit: false});
				this.fetchProducts()
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	onShowVerify = (item) => {
		this.setState({
			itemsOne: item,
			selectedImage: item.featuredImage,
			id: item._id,
			code: item.code,
			stockType: item.stockType,
			edit: true
		})
	}

	render() {
		const { products, pages, loading, company, vendors, verify, itemsOne, selectedImage } = this.state
		let vendorname = ''
		if (itemsOne.vendor) {
			if (itemsOne.vendor.vendor) {
				vendorname = itemsOne.vendor.vendor.name
			}
		}
		const columns = [
			{
		    Header: 'No',
		    filterable: false,
		    minWidth: 75,
		    Cell: props => <span>{props.index + 1}</span>
		  },
		  {
		    Header: 'Gambar',
		    accessor: 'featuredImage',
		    filterable: false,
		    Cell: props => <div>	
							{
								props.value !== undefined && props.value !== null ?
									<img src={`${BASE_URL}/img/${props.value}`} alt="product" style={{width: 150, height: 150}}/>	
								:
		      	  		<img src="/img/default_product.png" alt="product" style={{width: 150, height: 150}}/>
							}
						</div>
		  },{
		    Header: 'Kode Produk',
		    accessor: 'code'
		  },{
		    Header: 'Nama',
		    accessor: 'name'
		  }
		  ,{
		    Header: 'Harga',
		    accessor: 'price',
		    filterable: false
		  }
		  ,{
		    Header: 'Brand',
		    accessor: 'brand'
		  },{
		    Header: 'Vendor',
		    filterable: false,
		    accessor: 'vendor.vendor.name'
		  },{
		    Header: 'type',
		    accessor: 'stockType'
		  },{
		    Header: 'Spec',
		    accessor: 'specification'
		  },{
		    Header: 'Status',
		    filterable: false,
		    accessor: 'code',
		    Cell: props => <div>	
							{
								props.value !== null ?
									<label><span className="fa fa-check-double" style={{color: 'green'}}></span> Terverifikasi</label>
								:
		      	  		<label><span className="fa fa-check" style={{color: 'red'}}></span> Belum Terverifikasi</label>
							}
						</div>
		  }, {
	    Header: 'Aksi',
	    filterable: false,
	    Cell: props => (
           <div>
           <center>
               <button 
               		onClick={() => this.onShowVerify(props.original)}
									className="button is-outlined is-small is-info is-rounded"
								>
									<span className="icon">
									  <i className="fas fa-pencil-alt"></i>
									</span>
									&nbsp;
									{
										(props.original.code === null) ?
										' Verifikasi'
										:
										' Update Kode'
									}
								</button>
            </center>
           </div>
       )
	  }
		]
		return (
			<section className="section">
				<div className="container">
					<div className="columns">
						<div className="column">
							<div className="columns">
								<div className="column">
									<h5 className="title is-5">
										List Produk
									</h5>
								</div>
							</div>
							<div className="columns">
								<div className="column is-4">
									<div className="field">
									  <label className="label">Filter By Vendor</label>
									  <div className="control">
									    <select 
									    	className="input"
									    	name="company" 
									    	onChange={this.handleInputChange} 
									    	value={company ? company : ''}
									    >
									    	<option value="">- Semua Vendor -</option>
									    	{
									    		vendors.map((item, index) => (
									    				<option value={`${item._id}`}>{item.name}</option>		
									    			)
									    		)
									    	}
									    </select>
									  </div>
									</div>
								</div>
								<div className="column is-4">
									<div className="field">
									  <label className="label">Status Material</label>
									  <div className="control">
									    <select 
									    	className="input"
									    	name="verify" 
									    	onChange={this.handleInputChangeVerify} 
									    	value={verify ? verify : ''}
									    >
									    	<option value="">- Semua -</option>
									    	<option value="0">Terverifikasi</option>
									    	<option value="1">Belum Terverifikasi</option>
									    </select>
									  </div>
									</div>
								</div>
							</div>
							<ReactTable
								manual
						    data={products}
						    columns={columns}
						    filterable
						    pages={pages}
						    loading={loading}
			          defaultPageSize={10}
			          className="-highlight"
			          onFetchData={this.fetchProducts}
						  />
						</div>
					</div>					
				</div>
				<div className={`modal ${this.state.edit && 'is-active'}`}>
				  <div className="modal-background" onClick={() => this.setState({edit: false})}></div>
				  <div className="modal-content">
				    <div className="box">
				    	<div className="content">
				    		<h5 className="title is-5">Verifikasi</h5>
				    		<div style={{width: '100%'}}>
									<div className="columns">
										<div className="column">
											<figure className="image is-square card">
											  {
						      	  		selectedImage ? 
						      	  			<img src={`${BASE_URL}/img/${selectedImage}`} alt="produk" style={{objectFit: 'contain'}}/>
						      	  		:
						      	  			<img src="/img/default_product.png" alt="product" style={{objectFit: 'cover'}}/>
						      	  	}
											</figure>
											<div className="columns is-gapless is-mobile">
												{
													itemsOne.images && itemsOne.images.map((item, index) => (
														<div className="column" key={index}>
															<div className="card">
																<Link to="#" onClick={() => this.selectImage(item)}>
																	<figure className="image is-square" style={{marginLeft: 0, marginRight: 0}}>
																	  {
																	  	item ?
																	  		<img src={`${BASE_URL}/img/${item}`} alt="produk" style={{objectFit: 'cover'}}/>
																  		:
																  			<img src="/img/default_product.png" alt="product" style={{objectFit: 'cover'}}/>
																	  }
																	</figure>
																</Link>
															</div>
														</div>
													))
												}
											</div>
										</div>
										<div className="column is-4">
											<h4 className="title is-4">
												{itemsOne.name}
												&nbsp;
												{
													authorize.canMenuVendor() &&
													authorize.belongs(itemsOne.vendor && itemsOne.vendor.companyName, 'companyName') &&
														<Link 
															to={`/my-products/${this.state.id}/edit`} 
															className="button is-primary is-small is-outlined is-rounded"
														>
															<span className="icon">
																<i className="fa fa-pencil-alt"></i>
															</span>
															&nbsp;
															Edit
														</Link>
												}
											</h4>
											<b className="has-text-primary">
												{numeral(itemsOne.price).format('$ 0,0')}
											</b>
											<br/>
											Kode:&nbsp;
											{
												this.state.updated ?
													this.state.code
												: 
													itemsOne.code || <button className="button is-small is-rounded is-warning tooltip" data-tooltip="Menunggu Verifikasi">--</button>
											}
											<br/>
											<p style={{fontSize: 12, marginTop: 10}}>
												UoM: {itemsOne.uom}
												<br/>
												Brand: {itemsOne.brand}
												<br/>
												Vendor: {vendorname}
												<br/>
												Tipe: {itemsOne.stockType === 'nonstock' ? 'Non Stock (N)' : 'Stock (L)'}
											</p>
											<hr/>
											

											<b>Full Specification</b>
											{
												<p className="content is-small" style={{marginTop: 10}}>
													{itemsOne.specification && itemsOne.specification.split('\n').map((item2, key2) => {
													  return <span key={key2}>{item2}<br/></span>
													})}
												</p>
											}
										</div>
									</div>
								</div>
								<br />
				    		<br />
				    		<div className="field">
				    		  <label className="label">Material Number / Kode Identifikasi</label>
		    		      <div className="control">
		    		        <input className="input" type="text" name="code" onChange={this.handleInputChange} value={this.state.code ? this.state.code : ''} />
		    		      </div>
				    		</div>
				    		<div className="field">
				    		  <label className="label">Tipe</label>
		    		      <div className="control">
		    		        <label className="radio">
		    		          <input 
		    		          	type="radio" 
		    		          	name="stockType" 
		    		          	value="stock" 
		    		          	onChange={this.handleInputChange} 
		    		          	checked={this.state.stockType === 'stock'}
		    		          />
		    		          &nbsp;
		    		          Stok
		    		        </label>
		    		        <label className="radio">
		    		          <input 
		    		          	type="radio" 
		    		          	name="stockType" 
		    		          	value="nonstock" 
		    		          	onChange={this.handleInputChange} 
		    		          	checked={this.state.stockType === 'nonstock'}
		    		          />
		    		          &nbsp;
		    		          Non Stok
		    		        </label>
		    		      </div>
				    		    
				    		</div>
				    	
				    		<div className="field is-grouped is-pulled-right">
				    			<div className="control">
				    			  <button onClick={() => this.setState({edit: false})} className="button is-text">Cancel</button>
				    			</div>
				    		  <div className="control">
				    		    <button className={`button is-link is-rounded`} onClick={this.handleSubmit}>Simpan</button>
				    		  </div>
				    		</div>

				    		<br/>
				    		<br/>

				    	</div>
				    </div>
				  </div>
				  <button className="modal-close is-large" aria-label="close" onClick={() => this.setState({edit: false})}></button>
				</div>
			</section>
		);
	}
}

export default PicProduct
