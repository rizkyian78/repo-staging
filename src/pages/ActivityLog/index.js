import React, { Component } from 'react';
import ReactTable from 'react-table'
import moment from 'moment'
import 'react-table/react-table.css'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'

class ActivityLog extends Component {
	state = {
		labelActive:0,
		activitylog: [],
		load: false,
		loading: true,
		currentPage: 1,
		pages: null,
		perPage: 10,
	}

	componentDidMount() {
		if (auth.getRole() !== 9) {
			this.props.history.push('/login')
		}
	}

	fetchActivity = async (state, instance) => {
		const { page, pageSize, filtered} = state;
		const filterString = JSON.stringify(filtered);
    let params = `?page=${page}&pageSize=${pageSize}&filtered=${filterString}`
    const paramsEncode = encodeURI(params)
		apiCall.get(`/activity-log${paramsEncode}`)
		.then((response) => {
			this.setState({
				activitylog: response.data.data,
				currentPage: page,
        perPage: pageSize,
        pages: parseInt(response.data.totalRow / pageSize, 10) + (response.data.totalRow % pageSize > 0 ? 1 : 0),
        loading: false
			});
		})
		.catch((err) => {
			console.log(err.message)
		})
	}

	renderProduct = () => {		
		const { pages, loading } = this.state
		const columns = [{
	    Header: 'No',
	    filterable: false,
	    minWidth: 75,
	    Cell: props => <span>{props.index + 1}</span>
	  }, {
	    Header: 'Nama Table',
	    accessor: 'table'
	  }, {
	    Header: 'Action',
	    accessor: 'action'
	  }, {
	    Header: 'Deskripsi',
	    accessor: 'desc'
	  }, {
	    Header: 'IP Address',
	    accessor: 'ipAddress'
	  }, {
	    Header: 'Nama Komputer',
	    accessor: 'computerName'
	  }, {
	    Header: 'Petugas',
	    accessor: 'refUser.name'
	  }, {
	    Header: 'Tanggal',
	    accessor: 'createdAt',
	    Cell: props => (<div>{moment(props.value).format('DD MMM YYYY')}</div>)
	  }]
		return (
				<ReactTable
					manual
			    data={this.state.activitylog}
			    columns={columns}
			    filterable
          pages={pages}
			    loading={loading}
          defaultPageSize={10}
          className="-highlight"
          onFetchData={this.fetchActivity}
			  />
			);
	}

	render() {
		return (
			<section className="section">
				<div className="container">
					<div className="columns is-mobile">
						<div className="column">
							<div className="is-pulled-right">
								List Product
							</div>
						</div>
					</div>
					<div className="table-container">
						{this.renderProduct()}
					</div>
				</div>
			</section>
		);
	}
}

export default ActivityLog
