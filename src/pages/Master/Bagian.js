import React, { Component } from 'react';
import swal from 'sweetalert2'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import apiCall from '../../services/apiCall'

class Bagian extends Component {
	state = {
		data: [],
		modal: false,
		name: '',
		dataFungsi: [],
		refFungsi: '',
		id: '',
		loading: true,
		currentPage: 1,
		pages: null,
		perPage: 10,
	}

	componentDidMount() {
		this.getDataFungsiUsers()
	}

	getDataBagianUsers = (state, instance) => {
		const { page, pageSize, filtered, sorted } = state ? state : this.state;
    const filterString = JSON.stringify(filtered);
    const sort = JSON.stringify(sorted);
    let params = `?page=${page}&pageSize=${pageSize}&sorted=${sort}&filtered=${filterString}`
    const paramsEncode = encodeURI(params)
		apiCall.get(`/bagian${paramsEncode}`)
		.then((res) => {
			this.setState({
				data: res.data.data,
				currentPage: page,
        perPage: pageSize,
        pages: parseInt(res.data.totalRow / pageSize, 10) + (res.data.totalRow % pageSize > 0 ? 1 : 0),
        loading: false,
        page, pageSize, filtered, sorted
			})
		})
		.catch((error) => {
			console.log(error)
		})
	}

	getDataFungsiUsers = () => {
		apiCall.get('/fungsi')
		.then((response) => {
			this.setState({dataFungsi: response.data.data})
		})
		.catch((error) => {
			console.log(error.message)
		})
	}

	setAvailable = () => {
	  	this.setState({modal: !this.state.modal, id: '', name: '', refFungsi: ''})
	  }

	  onModal = () => {
	  	this.setAvailable()
	 }

	 changeText = (e) => {
	 	const target = e.target
	 	this.setState({
	 		name: target.value
	 	})
	 }

	 onSubmit = (e) => {
	 	e.preventDefault()
	 	if (this.state.id === '') {
	 		this.onInsertData()
	 	} else {
	 		this.onUpdateData()
	 	}
	 }

	 onInsertData = () => {
	 	const formData = {name: this.state.name, refFungsi: this.state.refFungsi}
	 	apiCall.post('/bagian/create',{
	 		formData
	 	})
	 	.then((response) => {
	 		this.setState({name: '', modal: false, refFungsi: ''})
	 		this.getDataBagianUsers()
	 		if (response.status === 200) {
	 			swal.fire('Sukses menambahkan data', '', 'success')
	 		} else {
	 			swal.fire('Gagal menambahkan data', '', 'warning')
	 		}
	 	})
	 	.catch((error) => {
	 		swal.fire(error.message, '', 'warning')
	 	})
	 }

	 onUpdateData = () => {
	 	const formData = {name: this.state.name, refFungsi: this.state.refFungsi}
	 	swal.fire({
		  title: 'Anda Yakin?',
		  text: "Akan mengubah data bagian",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, ubah',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.value) {
		  	apiCall.put(`/bagian/update/${this.state.id}`,{
			 		formData
			 	})
			 	.then((response) => {
			 		this.setState({name: '', modal: false, id: ''})
			 		this.getDataBagianUsers()
			 		if (response.status === 200) {
			 			swal.fire('Sukses mengupdate data', '', 'success')
			 		} else {
			 			swal.fire('Gagal mengupdate data', '', 'warning')
			 		}
			 	})
			 	.catch((error) => {
			 		swal.fire(error.message, '', 'warning')
			 	})
		  }
		})
	 }

	 onShowUpdate = (item) => {
	 	this.setState({
	 		id: item._id,
	 		name: item.name,
	 		refFungsi: item.refFungsi._id,
	 		modal: true
	 	})
	 }

	 onDelete = (item) => {
	 	swal.fire({
		  title: 'Anda Yakin?',
		  text: "Akan menghapus data bagian",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, hapus',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.value) {
		  	apiCall.delete(`/bagian/delete/${item._id}`)
			 	.then((response) => {
			 		swal.fire('Sukses menghapus data', '', 'success')
			 		this.getDataBagianUsers()
			 	})
			 	.catch((error) => {
			 		swal.fire(error.message, '', 'warning')
			 	})  
		  }
		})
	 }

	render() {
		const columns = [{
	    Header: 'No',
	    filterable: false,
	    minWidth: 75,
	    Cell: props => <span>{props.index + 1}</span>
	  }, {
	    Header: 'Nama',
	    accessor: 'name'
	  }, {
	    Header: 'Fungsi',
	    accessor: 'refFungsi.name'
	  }, {
	    Header: 'Aksi',
	    filterable: false,
	    Cell: props => (
           <div>
           <center>
               <button 
               		onClick={() => this.onShowUpdate(props.original)}
									className="button is-outlined is-small is-info is-rounded"
								>
									<span className="icon">
									  <i className="fas fa-pencil-alt"></i>
									</span>
									&nbsp;
									Edit
								</button>{' '}
								<button 
									onClick={() => this.onDelete(props.original)}
									className="button is-outlined is-small is-danger is-rounded"
								>
									<span className="icon">
									  <i className="fa fa-times"></i>
									</span>
									&nbsp;
									Drop
								</button>
            </center>
           </div>
       )
	  }]

		return (
			<section className="section">
				<div className="container">
					<div className="columns is-mobile">
						<div className="column">
							<div className="is-pulled-right">
								<button className="button is-outlined is-small is-rounded is-info" onClick={() => this.onModal()}>+ Tambah</button>
							</div>
						</div>
					</div>
					<div className="table-container">
						<ReactTable
							manual
					    data={this.state.data}
					    columns={columns}
					    filterable
		          pages={this.state.pages}
					    loading={this.state.loading}
		          defaultPageSize={10}
		          className="-highlight"
		          onFetchData={this.getDataBagianUsers}
					  />
					</div>
				</div>
				<div class={this.state.modal ? 'modal is-active' : 'modal'}>
				  <div class="modal-background"></div>
				  <div class="modal-card">
				    <header class="modal-card-head">
				      <p class="modal-card-title">Form Bagian</p>
				      <button class="delete" aria-label="close" onClick={() => this.setAvailable()}></button>
				    </header>
				    <section class="modal-card-body">
				       <form onSubmit={this.onSubmit}>
				       		<div class="field">
									  <label class="label">Fungsi</label>
									  <div class="control">
									    <div class="select">
									      <select onChange={(e) => this.setState({refFungsi: e.target.value})} value={this.state.refFungsi}>
									        <option>Pilih Fungsi</option>
									        {
									        	this.state.dataFungsi.map((item, index) => (
									        		<option value={item._id} key={index.toString()}>{item.name}</option>		
									        		)
									        	)
									        }
									      </select>
									    </div>
									  </div>
									</div>
				       		<div class="field">
									  <label class="label">Nama Bagian</label>
									  <div class="control">
									    <input class="input" type="text" onChange={this.changeText} value={this.state.name} placeholder="Masukkan bagian" />
									  </div>
									</div>
									<button type="submit" class="button is-success">Simpan</button>
				       </form>
				    </section>
				    <footer class="modal-card-foot">
				    </footer>
				  </div>
				</div>
			</section>
		);
	}
}

export default Bagian
