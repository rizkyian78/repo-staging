import React, { Component } from 'react';
import swal from 'sweetalert2'
import auth from '../../services/auth'

class Login extends Component {
	state = {
		email: '',
		password: '',
		loading: false
	}

	componentDidMount() {
		const _idverify = localStorage.getItem('_idverify')
		if (_idverify) {
			window.location = '/'
		}
	}

	handleChange = (e) => {
		let name = e.target.name
		let value = e.target.value

		this.setState({[name]: value});
	}

	onSubmit = async (e) => {
		this.setState({loading: true});
		e.preventDefault()

		auth.login({
			email: this.state.email,
			password: this.state.password
		})
		.then(res => {
			const token = res.data.token
			localStorage.setItem('token', token)
			localStorage.setItem('_idverify', res.data._id)
			localStorage.setItem('_vendor', res.data.vendor)
			window.location ='/'

			this.setState({loading: false});
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
		
	}

	render() {
		return (
			<section className="section is-medium" style={{background: 'url("/img/bg.jpg") center', backgroundSize: 'cover'}}>
				<div className="container">
					<div className="columns">
						<div className="column is-4 is-offset-4">
							<div className="box">
								<h5 className="title is-5 has-text-centered">
									<img src="/img/logo.png" width="50" height="28" alt="logo" />
									<br/>
									LOGIN
								</h5>
								<hr />
								<form onSubmit={this.onSubmit}>
									<div className="field">
									  <label className="label">Email</label>
									  <div className="control has-icons-left">
									    <input className="input" type="email" name="email" onChange={this.handleChange} />
									    <span className="icon is-small is-left">
									      <i className="fas fa-envelope"></i>
									    </span>
									    {
									    	// <p className="help">Akun Vendor: vendor1@gmail.com</p>
									    	// <p className="help">Akun PIC Analyst: picanalyst1@gmail.com</p>
									    }
									  </div>
									</div>
									<div className="field">
									  <label className="label">Password</label>
									  <div className="control has-icons-left">
									    <input className="input" type="password" name="password" onChange={this.handleChange} />
									    <span className="icon is-small is-left">
									      <i className="fas fa-lock"></i>
									    </span>
									    {
									    	// <p className="help">password: 123123</p>
									    }
									  </div>
									</div>
									<br/>
									<div className="field">
									  <button className={`button is-primary is-fullwidth ${this.state.loading && 'is-loading'}`} type="submit">
									  	Login
									  </button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default Login
