import React, { Component } from 'react';
import { connect } from 'react-redux'
import swal from 'sweetalert2'
import numeral from 'numeral'

import apiCall from '../../services/apiCall'
import * as actions from '../../redux/actions'
import auth from '../../services/auth'

class Cart extends Component {
	state = {
		loading: false
	}

	handleChange = (e, product) => {
		this.props.changeQuantity(product, Number(e.target.value))
	}

	sumValue = () => {
		let sum = 0
		for(let item of this.props.cart) {
			sum += (item.product.price * item.quantity)
		}

		return sum
	}

	createOrder = () => {
		if (!auth.isAuth()) {
			swal.fire('Login dulu untuk lanjut', '', 'info')
		}
		else if (this.props.cart.length === 0) {
			swal.fire('Silahkan pilih produk dulu', '', 'info')
		}
		else if(this.sumValue() > 50000000) {
			swal.fire('Melebihi Rp 50.000.000', 'Total tidak boleh lebih dari Rp 50.000.000', 'error')
		}
		else {
			this.setState({loading: true});

			apiCall.post('/orders', {
				formData: this.props.cart,
			})
			.then(res => {
				this.setState({loading: false});
				swal.fire('Sukses membuat order', '', 'success')

				this.props.history.push('/orders')
				this.props.emptyCart()
			})
			.catch(err => {
				if (err.response) {
					swal.fire(err.response.data.message, '', 'error')
					console.log(err.response.data.error)
				} else {
					swal.fire('Terjadi kesalahan pada sistem', '', 'error')
				}
				this.setState({loading: false});
			})
		}
	}

	createOrderFast = () => {
		if (!auth.isAuth()) {
			swal.fire('Login dulu untuk lanjut', '', 'info')
		}
		else if (this.props.cart.length === 0) {
			swal.fire('Silahkan pilih produk dulu', '', 'info')
		}
		else if(this.sumValue() > 50000000) {
			swal.fire('Melebihi Rp 50.000.000', 'Total tidak boleh lebih dari Rp 50.000.000', 'error')
		}
		else {
			this.setState({loading: true});

			apiCall.post('/orders-fast', {
				formData: this.props.cart,
			})
			.then(res => {
				this.setState({loading: false});
				swal.fire('Sukses membuat order', '', 'success')

				this.props.history.push('/orders')
				this.props.emptyCart()
			})
			.catch(err => {
				if (err.response) {
					swal.fire(err.response.data.message, '', 'error')
					console.log(err.response.data.error)
				} else {
					swal.fire('Terjadi kesalahan pada sistem', '', 'error')
				}
				this.setState({loading: false});
			})
		}
	}

	render() {
		const {cart} = this.props

		return (
			<section className="section">
				<div className="container">
					
					<div className="columns">
						<div className="column is-10 is-offset-1">
							<div className="card">
								<div className="card-content">
									<div className="content is-small">
										<div className="columns is-mobile">
											<div className="column is-8">
												<h6 className="title is-6">Keranjang Belanja</h6>
											</div>
											<div className="column">
												<div className="card">
													<div className="card-content has-text-centered">
														{
															cart.length > 0 &&
															cart[0].product.stockType
														}
													</div>
												</div>
											</div>
										</div>

										<table className="table is-narrow is-bordered">
											<thead>
												<tr>
													<th>No.</th>
													<th>Kode Barang</th>
													<th>Deskripsi Barang</th>
													<th>Qty. Demand</th>
													<th>Unit Price</th>
													<th>Subtotal</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												{
													cart.length > 0 && cart.map((item, index) => (
														<tr key={index}>
															<td>{index + 1}</td>
															<td>{item.product.code}</td>
															<td>{item.product.name}</td>
															<td>
																<input type="number" value={item.quantity} onChange={(e) => this.handleChange(e, item.product)} />
															</td>
															<td>{numeral(item.product.price).format('$ 0,0')}</td>
															<td>{numeral(item.product.price * item.quantity).format('$ 0,0')}</td>
															<td>
																<button onClick={() => this.props.deleteItem(item.product)} className="button is-small is-rounded">
																	Hapus
																</button>
															</td>
														</tr>
													))
												}
												<tr>
													<th colSpan="5">Total</th>
													<th>{numeral(this.sumValue()).format('$ 0,0')}</th>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<br/>
							<div className="has-text-right">
								<button
									className={`button is-rounded is-primary ${this.state.loading && 'is-loading'}`}
									onClick={this.createOrder}
									data-tooltip="Order dengan approval bertahap"
								>
									Buat Order
								</button>
							</div>
						</div>
						<div className="column">
							
						</div>
					</div>
				</div>
			</section>
		);
	}
}

const mapStateToProps = ({cart}) => {
  return {cart}
}

export default connect(mapStateToProps, actions)(Cart)
