import React, { Component } from 'react';
import swal from 'sweetalert2'

import VendorForm from './VendorForm'

import apiCall from '../../services/apiCall'

class VendorEdit extends Component {
	state = {
		data: {}
	}

	componentDidMount() {
		const id = this.props.location.pathname.split('/')[2]

		apiCall.get(`/vendors/${id}`)
		.then(res => {
			this.setState({
				data: res.data.data,
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	render() {
		return (
			<section className="section">
				<div className="container">
					<h5 className="title is-5 has-text-centered">Edit Vendor</h5>
					<div className="columns">
						<div className="column is-6 is-offset-3">
							<VendorForm
								edit
								history={this.props.history}
								formData={this.state.data}
							/>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default VendorEdit
