import React, { Component } from 'react';
import swal from 'sweetalert2'

import apiCall from '../../services/apiCall'

class ChangePassword extends Component {
	state = {
		loading: false,
		formData: {},
		vendors: []
	}

	componentDidMount() {
		console.log(this.props)
	}


	handleInputChange = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;
		this.setState({[name]: value})
	}

	handleSubmit = async (e) => {
		e.preventDefault()
		this.setState({loading: true});
		const id = localStorage.getItem('_idverify')
		apiCall.get(`/users/${id}`)
		.then(res => {
			this.setState({loading: false});
			const user = res.data.data
			this.changePassword(id, user)
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})

	}

	changePassword = (id, user) => {
		const { password_new, password_confir, password_old } = this.state
		if (user.password !== password_old) {
			swal.fire('Password lama tidak di temukan', '', 'warning')
		} else if (password_new !== password_confir) {
			swal.fire('Confirmasi password tidak sama', '', 'warning')
		} else {	
			swal.fire({
			  title: 'Ubah Password ?',
			  text: '',
			  type: 'info',
			  showCancelButton: true,
			  confirmButtonText: 'Ya, ubah',
			  cancelButtonText: 'Tidak '
			}).then((result) => {
			  if (result.value) {
			    this.updatePassword(id, password_new)
			  }
			})
		}
	}

	updatePassword = (id, password_new) => {
		apiCall.put(`/users/ubah-pass/${id}`, {
		password: password_new,
		})
		.then(res => {
			swal.fire('Sukses ubah password', '', 'success')
			.then(() => {
				window.location = '/'
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	render() {
		const { loading } = this.state

		return (
			<section className="section">
				<div className="container">
					<h5 className="title is-5 has-text-centered">Ubah Password</h5>
					<div className="columns">
						<div className="column is-4 is-offset-4">
						<form onSubmit={this.handleSubmit}>
							<div className="box">
								<div className="field">
								  <label className="label">Password Lama</label>
								  <div className="control">
								    <input 
								    	className="input" 
								    	name="password_old" 
								    	type="password" 
								    	onChange={this.handleInputChange} 
								    	value={this.state.password_old}
								    	required
								    />
								  </div>
								</div>
								<hr />
								<div className="field">
								  <label className="label">Password Baru</label>
								  <div className="control">
								    <input 
								    	required
								    	className="input" 
								    	name="password_new" 
								    	type="password" 
								    	onChange={this.handleInputChange} 
								    	value={this.state.password_new}
								    />
								  </div>
								</div>

								<div className="field">
								  <label className="label">Ulangi Password</label>
								  <div className="control">
								    <input 
								    	className="input" 
								    	name="password_confir" 
								    	type="password" 
								    	onChange={this.handleInputChange} 
								    	required
								    	value={this.state.password_confir}
								    />
								  </div>
								</div>
								<hr/>
								<div className="field is-grouped is-pulled-right">
								  <div className="control">
								    <button type="submit" className={`button is-link is-rounded ${loading && 'is-loading'}`}>Simpan</button>
								  </div>
								</div>
								<br/>
								<br/>
							</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default ChangePassword
