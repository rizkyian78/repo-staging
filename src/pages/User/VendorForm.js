import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import swal from 'sweetalert2'
import moment from 'moment'

import apiCall from '../../services/apiCall'
import { BASE_URL } from '../../constants'

class VendorForm extends Component {
	state = {
		loading: false,
		formData: {},
		imagePreviewUrl: null,
		imageFiles: null
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.edit) {
			this.setState({
				formData: nextProps.formData
			});
		}
	}

	handleInputChange = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;

		let { formData } = this.state

		formData[name] = value
		this.setState({formData})
	}

	handleSubmit = () => {
		this.setState({loading: true});
		const { formData, imageFiles } = this.state

		if (this.props.edit) {
			this.update(formData, imageFiles)
		} else {
			this.post(formData, imageFiles)
		}
	}

	post = (formData, imageFiles) => {
		apiCall.post('/vendors', {
			formData,
			imageFiles
		})
		.then(res => {
			this.setState({loading: false});
			swal.fire('Sukses menambahkan data', '', 'success')
			this.setState({formData: {}, imagePreviewUrl: null})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	update = (formData, imageFiles) => {
		apiCall.put(`/vendors/${formData._id}`, {
			formData,
			imageFiles
		})
		.then(res => {
			this.setState({loading: false});
			swal.fire('Sukses edit data', '', 'success')
			.then(() => {
				window.location = `/users`
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	handleImageChange = (e) => {
		const { formData } = this.state
		let reader = new FileReader()
		let file = e.target.files[0]

		reader.onloadend = (ev) => {	
			let fileName = moment() + '.jpg'
			fileName = fileName.replace(/[ ]/g, "_")

			formData['images'] = fileName
			let imageFiles = ev.target.result
			let imagePreviewUrl = reader.result

			this.setState({ 
				formData,
				imageFiles,
	      imagePreviewUrl
	    })
		}

		reader.readAsDataURL(file)
	}

	renderImage = () => {
		const { imagePreviewUrl, formData } = this.state
		let view

		if(imagePreviewUrl !== null) {
			view = <img alt="default" src={`${imagePreviewUrl}`} style={{objectFit: 'contain'}} />
		}	else {
			if (formData.images) {
				view = <img alt="default" src={`${BASE_URL}/vendors/${formData.images}`} style={{objectFit: 'contain'}} />
			} else {
				view = <img alt="default" src="/img/add_product.png" style={{objectFit: 'cover'}} />
			}
		}
			
		return (
			<div className="column is-3">
				<label className="hoverCursor">
					<figure className="image is-square">	
						{view}
					</figure>
					
					<input 
						type="file" 
						style={{display:'none'}} 
						onChange={(e) => this.handleImageChange(e)}
						accept="image/*"
					/>
				</label>
			</div>
		)
	}

	render() {
		const { formData, loading } = this.state

		return (
			<div className="box">
				<div className="field">
				  <label className="label">Nama Vendor</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="name" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.name ? formData.name : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Bidang Usaha</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="business" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.business ? formData.business : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Nama Pejabat</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="contactPerson" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.contactPerson ? formData.contactPerson : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Nomer Telp</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="phone" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.phone ? formData.phone : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Jabatan</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="position" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.position ? formData.position : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Alamat Vendor</label>
				  <div className="control">
				    <textarea 
				    	className="input"
				    	name="address" 
				    	style={{height: 100}}
				    	onChange={this.handleInputChange} 
				    	value={formData.address ? formData.address : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Images</label>
				  <div className="control">
				    <div className="columns is-mobile">
				    	{this.renderImage()}
				    </div>
				  </div>
				</div>

				<hr/>

				<div className="field is-grouped is-pulled-right">
					<div className="control">
					  <Link to="/users" className="button is-text">Cancel</Link>
					</div>
				  <div className="control">
				    <button className={`button is-link is-rounded ${loading && 'is-loading'}`} onClick={this.handleSubmit}>Simpan</button>
				  </div>
				</div>
				<br/>
				<br/>
			</div>
		);
	}
}

export default VendorForm
