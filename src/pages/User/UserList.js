import React, { Component } from 'react';
import swal from 'sweetalert2'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'
import { BASE_URL } from '../../constants'
import './stylesUser.css'

const roles = [
			{key: 1, value: 'Vendor'},
			{key: 2, value: 'PIC Analyst'},
			{key: 3, value: 'PIC Bagian'},
			{key: 4, value: 'Section Head Inventory Control'},
			{key: 5, value: 'Manager Procurement'},
			{key: 6, value: 'Section Head Purchasing'},
			{key: 7, value: 'Section Head Bagian'},
			{key: 8, value: 'Manager Fungsi'},
			{key: 9, value: 'Admin'},
		]
class UserList extends Component {
	state = {
		users: [],
		labelActive:0,
		vendors: [],
		load: false,
		loading: true,
		currentPage: 1,
		pages: null,
		perPage: 10,
	}

	componentDidMount() {
		if (auth.getRole() !== 9) this.props.history.push('/login')
			
	}

	fetchUsers = (state, instance) => {
		const { page, pageSize, filtered, sorted } = state ? state : this.state;
    const filterString = JSON.stringify(filtered);
    const sort = JSON.stringify(sorted);
    let params = `?page=${page}&pageSize=${pageSize}&sorted=${sort}&filtered=${filterString}`
    const paramsEncode = encodeURI(params)
		apiCall.get(`/users${paramsEncode}`)
		.then(res => {
			this.setState({
				users: res.data.data,
				currentPage: page,
        perPage: pageSize,
        pages: parseInt(res.data.totalRow / pageSize, 10) + (res.data.totalRow % pageSize > 0 ? 1 : 0),
        loading: false,
        page, pageSize, filtered, sorted
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	onDeleteUser = (item) => {
	 	swal.fire({
		  title: 'Anda Yakin Hapus?',
		  text: "Data yang sudah di hapus tidak bisa di kembalikan, lanjutkan ?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, hapus',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.value) {
		  	apiCall.delete(`/users/${item._id}`)
			 	.then((response) => {
			 		swal.fire('Sukses menghapus data', '', 'success')
			 		this.fetchUsers()
			 	})
			 	.catch((error) => {
			 		swal.fire(error.message, '', 'warning')
			 	})  
		  }
		})
	 }

	 customFilter = ({filter, onChange }) => {
    return (
      <select
        onChange={event => onChange(event.target.value)}
        style={{ width: "100%" }}
        value={filter ? filter.value : "all"}
      > 
        <option value="all">Show All</option>
        {
        	roles.map((item) => (
        		<option value={item.key}>{item.value}</option>		
        	)
        	)
        }
      </select>
    );
  }

	renderUser = () => {
		const { pages, loading } = this.state
		const columns = [{
	    Header: 'No',
	    filterable: false,
	    minWidth: 75,
	    Cell: props => <span>{props.index + 1}</span>
	  }, {
	    Header: 'Tanda Tangan',
	    accessor: 'signatureImage',
	    filterable: false,
	    Cell: props => <div>{props.value !== '' && <img src={`${BASE_URL}/img/${props.value}`} alt="ttd" style={{height: 80}}/>}</div>
	  },{
	    Header: 'Nama',
	    accessor: 'name'
	  }, {
	    Header: 'Email',
	    accessor: 'email'
	  }, {
	    Header: 'Password',
	    accessor: 'password'
	  }, {
	    Header: 'Role',
	    accessor: 'textRole',
	    filterMethod: (filter, row) => {
        return row[filter.id] === filter.value;
      },
	    Filter: ({ filter, onChange }) => this.customFilter({filter, onChange })
	  }, {
	    Header: 'Jabatan',
	    accessor: 'jabatan'
	  }, {
	    Header: 'Fungsi',
	    accessor: 'fungsi.name'
	  }, {
	    Header: 'Bagian',
	    accessor: 'bagian.name'
	  },{
	    Header: 'Nama Vendor',
	    accessor: 'vendor.name'
	  }, {
	    Header: 'Aksi',
	    filterable: false,
	    minWidth: 150,
	    Cell: props => (
           <div>
           <center>
               <Link 
									to={`/users/${props.original._id}/edit`}
									className="button is-outlined is-small is-rounded"
								>
									<span className="icon">
									  <i className="fas fa-pencil-alt"></i>
									</span>
									&nbsp;
									Edit
								</Link>
								{' '}
								<button 
               		onClick={() => this.onDeleteUser(props.original)}
									className="button is-outlined is-small is-danger is-rounded"
								>
									<span className="icon">
									  <i className="fas fa-pencil-alt"></i>
									</span>
									&nbsp;
									Drop
								</button>
            </center>
           </div>
       )
	  }]
		return (
				<ReactTable
					manual
			    data={this.state.users}
			    columns={columns}
			    filterable
			    pages={pages}
			    loading={loading}
          defaultPageSize={10}
          className="-highlight"
          onFetchData={this.fetchUsers}
			  />
			)
	}

	render() {
		return (
			<section className="section">
				<div className="container">
					<div className="columns is-mobile">
						<div className="column">
							<h3>List User</h3>
						</div>
						<div className="column">
							<div className="is-pulled-right">
								<Link to="/users/create" className="button is-small is-info is-outlined tooltip" data-tooltip="Tambahkan User">
									<span className="icon">
									  <i className="fas fa-plus"></i>
									</span>
									&nbsp;
								 	User
								</Link>
							</div>
						</div>
					</div>
					<div className="table-container">
						{
							this.renderUser()
						}
					</div>
				</div>
			</section>
		);
	}
}

export default UserList
