import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import swal from 'sweetalert2'
import moment from 'moment'

import apiCall from '../../services/apiCall'
import { BASE_URL } from '../../constants'

const roles = [
	{value: 1, label: 'Vendor'},
	{value: 2, label: 'PIC Analyst'},
	{value: 3, label: 'PIC Bagian'},
	{value: 4, label: 'Section Head Inventory Control'},
	{value: 5, label: 'Manager Procurement'},
	{value: 6, label: 'Section Head Purchasing'},
	{value: 7, label: 'Section Head Bagian'},
	{value: 8, label: 'Manager Fungsi'},
	{value: 9, label: 'Admin'},
	{value: 10, label: 'General Manager (GM)'},
	{value: 11, label: 'Senior Manager Operation'},
	{value: 12, label: 'Manufacturing (SMOM)'}
]

class UserForm extends Component {
	state = {
		loading: false,
		formData: {},
		vendors: [],
		dataFungsi: [],
		dataBagian: [],
		imagePreviewUrl: null,
		signatureImage: null
	}

	componentDidMount() {
		this.fetchVendors()
		this.fetchFungsi()
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.edit) {
			this.setState({
				formData: nextProps.formData
			}, () => {
				this.fetchBagian(this.state.formData.fungsi)
			});
		}
	}

	fetchVendors = () => {
		apiCall.get(`/vendors`)
		.then(res => {
			this.setState({
				vendors: res.data.data,
			})
		})
		.catch(err => {
			console.log('')
		})
	}


	fetchFungsi = () => {
		apiCall.get(`/fungsi`)
		.then(res => {
			this.setState({
				dataFungsi: res.data.data,
			})
		})
		.catch(err => {
			console.log('')
		})
	}

	fetchBagian = (idFungsi) => {
		apiCall.get(`/bagian/by-fungsi/${idFungsi}`)
		.then(res => {
			this.setState({
				dataBagian: res.data.data
			})
		})
		.catch(err => {
			console.log('')
		})
	}

	handleInputChange = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;
		console.log(value, '===')
		let { formData } = this.state
		if (name === 'companyName') {
			formData['vendor'] = value	
		}
		if (name === 'fungsi') {
			this.fetchBagian(value)
		}
		formData[name] = value
		this.setState({formData})
	}

	handleSubmit = () => {
		this.setState({loading: true});
		const { formData, signatureImage } = this.state

		if (this.props.edit) {
			this.update(formData, signatureImage)
		} else {
			this.post(formData, signatureImage)
		}
	}

	post = (formData, signatureImage) => {
		apiCall.post('/users', {
			formData,
			signatureImage
		})
		.then(res => {
			this.setState({loading: false, signatureImage: null, imagePreviewUrl: null});
			swal.fire('Sukses menambahkan user', '', 'success')
			this.setState({formData: {}})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	update = async (formData, signatureImage) => {
		const ven = this.state.vendors
		const feed = ven.find(elment => elment._id === formData.companyName);
		if (!feed) {
			formData['companyName'] = ''
		}
		apiCall.put(`/users/${formData._id}`, {
			formData,
			signatureImage
		})
		.then(res => {
			this.setState({loading: false, signatureImage: null, imagePreviewUrl: null});
			swal.fire('Sukses edit user', '', 'success')
			.then(() => {
				window.location = `/users`
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire('Gagal update data', '', 'warning')
				console.log(err.response.data.message)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	handleImageChange = async (e) => {
		const { formData } = this.state
		let reader = new FileReader()
		let file = e.target.files[0]

		reader.onloadend = (ev) => {	
			let fileName = moment() + '.jpg'

			formData['signatureImage'] = fileName
			let signatureImage = ev.target.result
			let imagePreviewUrl = reader.result

			console.log(formData)
			this.setState({ 
				formData,
				signatureImage,
	      imagePreviewUrl
	    })
		}

		reader.readAsDataURL(file)
	}

	renderImage = () => {
		const { imagePreviewUrl, formData } = this.state
		let view

		if(imagePreviewUrl !== null) {
			view = <img alt="default" src={`${imagePreviewUrl}`} style={{objectFit: 'contain'}} />
		}	else {
			if (formData.signatureImage) {
				view = <img alt="default" src={`${BASE_URL}/img/${formData.signatureImage}`} style={{objectFit: 'contain'}} />
			} else {
				view = <img alt="default" src="/img/add_product.png" style={{objectFit: 'cover'}} />
			}
		}
			
		return (
			<div className="column is-3">
				<label className="hoverCursor">
					<figure className="image is-square">	
						{view}
					</figure>
					
					<input 
						type="file" 
						style={{display:'none'}} 
						onChange={(e) => this.handleImageChange(e)}
						accept="image/*"
					/>
				</label>
			</div>
		)
	}

	render() {
		const { formData, loading } = this.state

		return (
			<div className="box">
				<div className="field">
				  <label className="label">Nama</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="name" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.name ? formData.name : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Email</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="email" 
				    	type="email" 
				    	onChange={this.handleInputChange} 
				    	value={formData.email ? formData.email : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Password</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="password" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.password ? formData.password : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Role</label>
				  <div className="control">
				    <select className="input" value={formData.role ? formData.role : ''} onChange={this.handleInputChange} name="role">
				    	<option value="">- Pilih Role -</option>
				    	{
				    		roles.map((item, index) => (
				    			<option value={item.value}>[{item.value}] {item.label}</option>		
				    			)
				    		)
				    	}
				    </select>
				  </div>
				</div>

				<div className="field">
				  <label className="label">Jabatan</label>
				  <div className="control">
				    <input 
				    	className="input" 
				    	name="jabatan" 
				    	type="text" 
				    	onChange={this.handleInputChange} 
				    	value={formData.jabatan ? formData.jabatan : ''}
				    />
				  </div>
				</div>

				<div className="field">
				  <label className="label">Tanda Tangan</label>
				  <div className="control">
				    <div className="columns is-mobile">
				    	{this.renderImage()}
				    </div>
				  </div>
				</div>
	
				<hr/>
				<span className="tag is-warning">Data Pegawai Pertamina</span>
				<br/>
				<br/>

				<div className="field">
				  <label className="label">Fungsi</label>
				  <div className="control">
				    <select className="input" value={formData.fungsi ? formData.fungsi : ''} onChange={this.handleInputChange} name="fungsi">
				    	<option value="">- Pilih Fungsi -</option>
				    	{
				    		this.state.dataFungsi.map((item, index) => (
				    			<option value={item._id}>{item.name}</option>		
				    			)
				    		)
				    	}
				    </select>
				  </div>
				</div>

				<div className="field">
				  <label className="label">Bagian</label>
				  <div className="control">
				    <select className="input" value={formData.bagian ? formData.bagian : ''} onChange={this.handleInputChange} name="bagian">
				    	<option value="">- Pilih Fungsi -</option>
				    	{
				    		this.state.dataBagian.map((item, index) => (
				    			<option value={item._id}>{item.name}</option>		
				    			)
				    		)
				    	}
				    </select>
				  </div>
				</div>

				<hr/>
				<span className="tag is-warning">Data Vendor</span>
				<br/>
				<br/>

				<div className="field">
				  <label className="label">Nama Perusahaan</label>
				  <div className="control">
				    <select 
				    	className="input"
				    	name="companyName" 
				    	onChange={this.handleInputChange} 
				    	value={formData.companyName ? formData.companyName : ''}
				    >
				    	<option value="">- Pilih Perusahaan -</option>
				    	{
				    		this.state.vendors.map((item, index) => (
				    				<option value={`${item._id}`}>{item.name}</option>		
				    			)
				    		)
				    	}
				    </select>
				  </div>
				</div>

				<hr/>

				<div className="field is-grouped is-pulled-right">
					<div className="control">
					  <Link to="/users" className="button is-text">Cancel</Link>
					</div>
				  <div className="control">
				    <button className={`button is-link is-rounded ${loading && 'is-loading'}`} onClick={this.handleSubmit}>Simpan</button>
				  </div>
				</div>
				<br/>
				<br/>
			</div>
		);
	}
}

export default UserForm
