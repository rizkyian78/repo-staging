import React, { Component } from 'react';
import swal from 'sweetalert2'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'
import Loading from '../../components/Loading'
import './stylesUser.css'

const roles = [
			{key: 1, value: 'Vendor'},
			{key: 2, value: 'PIC Analyst'},
			{key: 3, value: 'PIC Bagian'},
			{key: 4, value: 'Section Head Inventory Control'},
			{key: 5, value: 'Manager Procurement'},
			{key: 6, value: 'Section Head Purchasing'},
			{key: 7, value: 'Section Head Bagian'},
			{key: 8, value: 'Manager Fungsi'},
			{key: 9, value: 'Admin'},
		]
class VendorList extends Component {
	state = {
		users: [],
		labelActive:0,
		vendors: [],
		load: false,
		loading: true,
		currentPage: 1,
		pages: null,
		perPage: 10,
	}

	componentDidMount() {
		if (auth.getRole() !== 9) this.props.history.push('/login')
	}

	fetchVendors = (state, instance) => {
		const { page, pageSize, filtered, sorted } = state ? state : this.state;
    const filterString = JSON.stringify(filtered);
    const sort = JSON.stringify(sorted);
    let params = `?page=${page}&pageSize=${pageSize}&sorted=${sort}&filtered=${filterString}`
    const paramsEncode = encodeURI(params)
		apiCall.get(`/vendors-all${paramsEncode}`)
		.then(res => {
			this.setState({
				vendors: res.data.data,
				currentPage: page,
        perPage: pageSize,
        pages: parseInt(res.data.totalRow / pageSize, 10) + (res.data.totalRow % pageSize > 0 ? 1 : 0),
        loading: false,
        page, pageSize, filtered, sorted
			})
		})
		.catch(err => {
			this.setState({load: false})
			if (err.response) {
				swal.fire(err.message, '', 'error')
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	onDelete = (item) => {
	 	swal.fire({
		  title: 'Anda Yakin Hapus?',
		  text: "Data yang sudah di hapus tidak bisa di kembalikan, lanjutkan ?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, hapus',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.value) {
		  	apiCall.delete(`/vendors/${item._id}`)
			 	.then((response) => {
			 		swal.fire('Sukses menghapus data', '', 'success')
			 		this.fetchVendors()
			 	})
			 	.catch((error) => {
			 		swal.fire(error.message, '', 'warning')
			 	})  
		  }
		})
	 }

	 customFilter = ({filter, onChange }) => {
    return (
      <select
        onChange={event => onChange(event.target.value)}
        style={{ width: "100%" }}
        value={filter ? filter.value : "all"}
      > 
        <option value="all">Show All</option>
        {
        	roles.map((item) => (
        		<option value={item.key}>{item.value}</option>		
        	)
        	)
        }
      </select>
    );
  }

	renderVendors = () => {
		const { pages, loading } = this.state
		const columns = [{
	    Header: 'No',
	    filterable: false,
	    minWidth: 75,
	    Cell: props => <span>{props.index + 1}</span>
	  }, {
	    Header: 'Nama',
	    accessor: 'name'
	  }, {
	    Header: 'Bidang Usaha',
	    accessor: 'business'
	  }, {
	    Header: 'Alamat',
	    accessor: 'address'
	  }, {
	    Header: 'Contact Person',
	    accessor: 'contactPerson'
	  }, {
	    Header: 'No Telp',
	    accessor: 'phone'
	  }, {
	    Header: 'Keterangan',
	    accessor: 'position',
	  }, {
	    Header: 'Aksi',
	    filterable: false,
	    Cell: props => (
           <div>
           <center>
               <Link 
									to={`/vendors/${props.original._id}/edit`}
									className="button is-outlined is-small is-rounded"
								>
									<span className="icon">
									  <i className="fas fa-pencil-alt"></i>
									</span>
									&nbsp;
									Edit
								</Link>
								{' '}
								<button 
               		onClick={() => this.onDelete(props.original)}
									className="button is-outlined is-small is-danger is-rounded"
								>
									<span className="icon">
									  <i className="fas fa-pencil-alt"></i>
									</span>
									&nbsp;
									Drop
								</button>
            </center>
           </div>
       )
	  }]
		return (
				<ReactTable
					manual
			    data={this.state.vendors}
			    columns={columns}
          filterable
			    pages={pages}
			    loading={loading}
          defaultPageSize={10}
          className="-highlight"
          onFetchData={this.fetchVendors}
			  />
			)
	}

	render() {
		return (
			<section className="section">
				<div className="container">
					<div className="columns is-mobile">
						<div className="column">
							<h2>List Vendor</h2>
						</div>
						<div className="column">
							<div className="is-pulled-right">
								<Link to="/users/create" className="button is-small is-info is-outlined tooltip" data-tooltip="Tambahkan User">
									<span className="icon">
									  <i className="fas fa-plus"></i>
									</span>
									&nbsp;
								 	vendor
								</Link>
							</div>
						</div>
					</div>
					<div className="table-container">
						{
							this.renderVendors()
						}
					</div>
				</div>
				<Loading load={this.state.load} />
			</section>
		);
	}
}

export default VendorList
