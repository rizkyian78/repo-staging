import React, { Component } from 'react';
import XLSX from 'xlsx'
import swal from 'sweetalert2'
import { Link } from 'react-router-dom'

import { BASE_URL } from '../../constants'
import apiCall from '../../services/apiCall'

class UserUpload extends Component {
	state = {
		users: [],
		loading: false
	}

	handleFileChange = (e) => {
		let file = e.target.files[0]
		let reader = new FileReader()

		reader.onload = e => {
			let data = reader.result
			let workbook = XLSX.read(data, {
				type: 'binary'
			})
			
			let users = []
			for(let i=0; i<workbook.SheetNames.length; i++) {
				let sheetName = workbook.SheetNames[i]
				let sheet = workbook.Sheets[sheetName]
				let rows = XLSX.utils.sheet_to_row_object_array(sheet)

				
				let fungsi = ''
				let bagian = null
				for(let key in rows) {
					if (rows[key]['Nama'] !== undefined && rows[key]['E-mail Pertamina'] !== undefined) {
						let rowData = {}
						rowData.jabatan = rows[key]['Jabatan']
						rowData.name = rows[key]['Nama']
						rowData.password = Math.floor(Math.random() * 999999)
						rowData.email = rows[key]['E-mail Pertamina']

						// ttd
						let zero1 = rows[key]['Tanda Tangan'] !== undefined ? (rows[key]['Tanda Tangan'].toString().length === 1 ? '00' : (rows[key]['Tanda Tangan'].toString().length === 2 ? '0' : '')) : ''
						let imageName1 = rows[key]['Tanda Tangan'] !== undefined ? `ttd-${zero1}${rows[key]['Tanda Tangan']}.png` : null
						rowData.signatureImage = imageName1

						// role
						if (rows[key]['Jabatan'].includes('Manager')) {
							rowData.role = 8
						}
						else if (rows[key]['Jabatan'].includes('Head') || rows[key]['Jabatan'].includes('Ast') || rows[key]['Jabatan'].includes('Lead of')) {
							rowData.role = 7
						}
						else if (rows[key]['Jabatan'].includes('PIC Pekerja')) {
							rowData.role = 3
						}
						
						// fungsi
						if (fungsi === '') {
							fungsi = rows[key]['__EMPTY'].split(': ')[1]
						}
						rowData.fungsi = fungsi

						// bagian
						let bagianTemp = rows[key]['__EMPTY_1'] || null
						if (bagianTemp) {
							bagian = bagianTemp
						}
						rowData.bagian = bagian

						users.push(rowData)
					}
				}
			}

			this.setState({users})
		}

		reader.onerror = function(ex) {
		  console.log(ex)
			swal.fire('Terdapat kesalahan pada file, hubungi admin', '', 'error')
		}

    reader.readAsBinaryString(file)
	}

	handleSubmit = () => {
		this.setState({loading: true});

		apiCall.post('/users-upload-excel', {
			formData: this.state.users
		})
		.then(res => {
			this.setState({loading: false, users: []});
			swal.fire('Sukses Upload Data', '', 'success')
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
			this.setState({loading: false});
		})
	}

	render() {
		const { users, loading } = this.state
		console.log(users)

		return (
			<div>
				<div className="box">
					<div className="field">
						<label className="label">
							Upload User (Excel)
							&nbsp;
							<a 
								href={`${BASE_URL}/files/Data Nama PIC, scan ttd, dan e-mail - USER.xlsx`} 
								className="button is-primary is-small is-outlined is-rounded is-pulled-right"
							>
								<span className="icon">
									<i className="fa fa-file-excel"></i>
								</span>
								&nbsp;
								Contoh File
							</a>
						</label>
						<div className="control">
							<div className="file">
							  <label className="file-label">
							    <input 
							    	className="file-input" 
							    	type="file" 
							    	name="file" 
							    	accept=".xlsx, .xls, .csv" 
							    	onChange={this.handleFileChange}
							    />
							    <span className="file-cta">
							      <span className="file-icon">
							        <i className="fas fa-upload"></i>
							      </span>
							      <span className="file-label">
							        Upload File...
							      </span>
							    </span>
							  </label>
							</div>
						</div>
					</div>
					{
						users.length > 0 &&
						<div>
							<h6 className="title is-6">Data dari file excel:</h6>
							<p>{users.length} User</p>

							<div className="field is-grouped is-pulled-right">
								<div className="control">
								  <Link to="/users" className="button is-text">Cancel</Link>
								</div>
							  <div className="control">
							    <button className={`button is-link is-rounded ${loading && 'is-loading'}`} onClick={this.handleSubmit}>Simpan</button>
							  </div>
							</div>
						</div>
					}
					
				</div>
				
			</div>
		);
	}
}

export default UserUpload
