import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import UserForm from './UserForm'
import UserUpload from './UserUpload'
import VendorForm from './VendorForm'

class UserCreate extends Component {
	state = {
		tabActive: 1
	}

	render() {
		const { tabActive } = this.state

		return (
			<section className="section">
				<div className="container">
					<h5 className="title is-5 has-text-centered">Tambahkan Data</h5>
					<div className="columns">
						<div className="column is-6 is-offset-3">
							<div className="tabs is-small">
								<ul>
									<li className={tabActive === 1 ? 'is-active' : ''}>
										<Link onClick={() => this.setState({tabActive: 1})} to="#">Users</Link>
									</li>
									<li className={tabActive === 4 ? 'is-active' : ''}>
										<Link onClick={() => this.setState({tabActive: 3})} to="#">Vendor</Link>
									</li>
									<li className={tabActive === 2 ? 'is-active' : ''}>
										<Link onClick={() => this.setState({tabActive: 2})} to="#">Banyak (Pegawai Pertamina)</Link>
									</li>
									{/*<li className={tabActive === 3 ? 'is-active' : ''}>
																			<Link onClick={() => this.setState({tabActive: 3})} to="#">Banyak (Vendor)</Link>
																		</li>*/}
								</ul>
							</div>
							
							{
								tabActive === 1 ?
									<UserForm />
								:
								tabActive === 2 ?
									<UserUpload />
								:
									<VendorForm />
							}
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default UserCreate