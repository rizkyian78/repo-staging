import React, { Component } from 'react';
import swal from 'sweetalert2'
import {Link} from 'react-router-dom'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'
import Loading from '../../components/Loading'

class OrderVendor extends Component {
	state = {
		orders: [],
		tabActive: 1,
		load: false,
		modal: false,
		idOrder: '',
		statusOrder: ''
	}

	componentDidMount() {
		if (!auth.isAuth()) 
			this.props.history.push('/login')
		else
			if(auth.getRole() !== 1){
				this.props.history.push('/')
			} else {
				this.fetchOrder()
			}
	}

	fetchOrder = (params) => {
		this.setState({load: true})
		apiCall.get(`/orders-by-vendor`, {})
		.then(res => {
			this.setState({
				orders: res.data.data,
				load: false
			})
		})
		.catch(err => {
			this.setState({load: false})
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	showModalUpdateStatus = (item) => {
		this.setState({
			idOrder: item._id, 
			statusOrder: item.statusOrder ? item.statusOrder: ''
		})
		this.onModalUpdateStatus()
	}

	onModalUpdateStatus = () => {
		this.setState({modal: !this.state.modal})
	}

	onUpdateStatus = () => {
		apiCall.put(`/orders/status/${this.state.idOrder}`, {
			statusOrder: this.state.statusOrder
		})
		.then(res => {
			this.setState({statusOrder: ''})
			this.onModalUpdateStatus()
			swal.fire('Success update status', '', 'success')
			this.fetchOrder()
		})
		.catch(err => {
			this.setState({load: false})
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	changeText = (e) => {
		// console.log(e.target)
		this.setState({[e.target.name]: e.target.value})
	}

	renderStatus = (order) => {
		let statuses = {
			stock: [
				'PIC Analyst', 'SH Inventory Control', 'Mgr. Procurement', 'SH. Purchasing', 'Vendor'
			],
			nonstock: [
				'PIC Bagian', 'SH Bagian', 'Mgr. Fungsi', 'SH. Purchasing', 'Vendor'
			]
		}

		let view = []
		for(let i=0; i< statuses[order.stockType].length; i++) {
			view.push(
				<li key={i}>
					{statuses[order.stockType][i]} : {order[`approval${i}`] === true ? <span className="tag is-success">Approved</span> : (order[`approval${i}`] === false ? <span className="tag is-danger">Rejected</span> : '')}
				</li>
			)
		}

		return view
	}

	deleteOrder = (id) => {
		swal.fire({
		  title: 'Yakin?',
		  text: "Data yang dihapus tidak bisa dikembalikan",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#d33',
		  cancelButtonColor: '#3085d6',
		  confirmButtonText: 'Hapus'
		}).then((result) => {
		  if (result.value) {
		  	apiCall.delete(`/orders/${id}`)
		  	.then(res => {
		  		swal.fire(
		  		  'Berhasil!',
		  		  'Data terhapus',
		  		  'success'
		  		)

					this.fetchOrder()
		  	})
		  	.catch(err => {
		  		if (err.response) {
		  			swal.fire(err.response.data.message, '', 'error')
		  			console.log(err.response.data.error)
		  		} else {
		  			swal.fire('Terjadi kesalahan pada sistem', '', 'error')
		  		}
		  	})
		  }
		})
	}

	render() {
		const {orders} = this.state

		return (
			<section className="section">
				<div className="container">
					<div className="columns">
						<div className="column">
							<h3>List Order Masuk</h3>
							<br />
							<div className="table-container">	
								<table className="table is-striped is-bordered is-fullwidth is-hoverable">
									<thead>
										<tr>
											<th>No.</th>
											<th>No. PR</th>
											<th>No. Registrasi</th>
											<th>PIC</th>
											<th>Items</th>
											<th>Status</th>
											<td>Aksi</td>
										</tr>
									</thead>
									<tbody>
										{
											orders.length > 0 &&
											orders.map((item, index) => (
												<tr key={index}>
													<td>{index + 1}</td>
													<td>{item.number}</td>
													<td>{item.registrationNumber}</td>
													<td>{item.createdBy ? item.createdBy.name : ''}</td>
													<td>
														<div className="content is-small">
															<table className="table is-narrow">
																<thead>
																	<tr>
																		<th>No.</th>
																		<th>Barang</th>
																		<th>Qty Demand</th>
																		<th>Qty Supply</th>
																	</tr>
																</thead>
																<tbody>
																	{
																		item.items.length > 0 &&
																		item.items.map((item2, index2) => (
																			<tr key={index2}>
																				<td>{index2 + 1}</td>
																				<td>{item2.product.name}</td>
																				<td>{item2.quantity}</td>
																				<td>{item2.quantitySupply}</td>
																			</tr>
																		))
																	}
																</tbody>
															</table>
														</div>
													</td>
													<td>
														<div className="content is-small">
															{
																	item.approval5By ?
																	<div>
																		<ol style={{marginLeft: 10}}>
																			<li>
																				{item.approval5By.fungsi} : {item[`approval5`] === true ? <span className="tag is-success">Approved</span> : (item[`approval5`] === false ? <span className="tag is-danger">Rejected</span> : '')}
																			</li>
																			<li>
																				SH. Purchasing : {item[`approval3`] === true ? <span className="tag is-success">Approved</span> : (item[`approval3`] === false ? <span className="tag is-danger">Rejected</span> : '')}
																			</li>
																			<li>
																				Vendor : {item[`approval4`] === true ? <span className="tag is-success">Approved</span> : (item[`approval4`] === false ? <span className="tag is-danger">Rejected</span> : '')}
																			</li>
																		</ol>
																		{
																			item.statusOrder &&
																			<label><b>Notes Vendor.</b> {item.statusOrder}</label>
																		}
																	</div>
																	:
																	<ol style={{marginLeft: 10}}>
																		{this.renderStatus(item)}
																		{
																			item.statusOrder &&
																			<li><b>{item.statusOrder}</b></li>
																		}
																	</ol>

																}
														</div>
													</td>
													<td align="center">
														<Link to={`/orders/${item._id}`} className="button is-small is-outlined is-rounded is-link">Detail</Link>
														<br/>
														<br/>
														{
															item.approval4 &&
															<button onClick={() => this.showModalUpdateStatus(item)} className="button is-small is-outlined is-rounded is-primary">Notes</button>
														}
													</td>
												</tr>
											))
										}
									</tbody>
								</table>
							</div>
							{
									orders.length === 0 &&
									<div className="columns">
										<div className="column has-text-center" style={{paddingTop: 150}}>
											<center>
												<label style={{fontSize: 30}}>Tidak Order Masuk</label>
											</center>
										</div>
									</div>
								}
						</div>
					</div>
				</div>
				<Loading load={this.state.load} />
				<div class={this.state.modal ? 'modal is-active' : 'modal'}>
				  <div class="modal-background"></div>
				  <div class="modal-card">
				    <header class="modal-card-head">
				      <p class="modal-card-title">Catatan Untuk Pertamina</p>
				      <button onClick={() => this.onModalUpdateStatus()} class="delete" aria-label="close"></button>
				    </header>
				    <section class="modal-card-body">
				      <form onSubmit={this.onSubmit}>
				      	<div class="field">
								  <label class="label">Status Order</label>
								  <div class="control">
								    <textarea class="input" type="text" name="statusOrder" value={this.state.statusOrder} placeholder="Masukkan Status Pemesanan" onChange={(e) => this.changeText(e)}></textarea>
								  </div>
								</div>
				      </form>
				    </section>
				    <footer class="modal-card-foot">
				      <button onClick={() => this.onUpdateStatus()} class="button is-success">Save changes</button>
				      <button onClick={() => this.onModalUpdateStatus()} class="button">Cancel</button>
				    </footer>
				  </div>
				</div>
			</section>
		);
	}
}

export default OrderVendor
