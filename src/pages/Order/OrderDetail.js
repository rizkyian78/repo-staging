import React, { Component } from 'react';
import { connect } from 'react-redux'
import moment from 'moment'
import numeral from 'numeral'
import swal from 'sweetalert2'
import Loading from '../../components/Loading'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'
import authorize from '../../services/authorize'
import * as actions from '../../redux/actions'
import { BASE_URL } from '../../constants'

class OrderDetail extends Component {
	state = {
		id: null,
		order: {},
		fixItems: [],
		reject: false,
		notes: '',
		printNote: false,
		orderNumber: false,
		logChangeQty: [],
		load: false,
		updateQty: false,
		sttNoPr: false
	}
	
	componentDidMount() {
		if (!auth.isAuth()) 
			this.props.history.push('/login')
		else
			this.fetchOrder()
			this.fetchOrderFix()
	}

	fetchOrder = () => {
		const id = this.props.location.pathname.split('/')[2]
		this.setState({id, load: true})

		apiCall.get(`/orders/${id}`)
		.then(res => {
			this.setState({
				order: res.data.data,
				orderNumber: res.data.data.number === null ? false : true,
				load: false
			}, () => {
				this.cekApprovelUser()
			})
		})
		.catch(err => {
			this.setState({load: false})
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	fetchOrderFix = () => {
		const id = this.props.location.pathname.split('/')[2]
		apiCall.get(`/orders/${id}`)
		.then(res => {
			this.setState({
				fixItems: res.data.data.items,
				sttNoPr: (res.data.data.number && res.data.data.approval0 !== null) ? res.data.data.number : null
			})
		})
		.catch(err => {
			
		})
	}

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}


	handleQtyChange = (e, product) => {
		let {order, logChangeQty, fixItems} = this.state
		const target = e.target
		const name = target.name
		const value = Math.floor(target.value)
		let index = order.items.findIndex(item => item.product._id === product._id)
		
		let searchIndex = logChangeQty.findIndex((item) => {
			return item === index
		})
		if (fixItems[index].quantity === value) {
			if (searchIndex >= 0) {
				logChangeQty.splice(searchIndex, 1)
			}
		} else {
			if (searchIndex < 0) {
				logChangeQty.push(index)
			}
		}
		order.items[index][name] = value
		this.setState({order: order, logChangeQty: logChangeQty});
	}

	handleInputChange = (e) => {		
		let {order} = this.state

		order.number = e.target.value
		this.setState({order});
	}

	handleChangeSubmit = () => {
		let {order} = this.state
		if(this.sumValue('quantity') > 50000000 || this.sumValue('quantitySupply') > 50000000) {
			swal.fire('Batas Pemesanan', 'Total tidak boleh lebih dari Rp 50.000.000', 'info')
		}
		else if(order.number === null) {
			swal.fire('Harap mengisi No. PR', '', 'info')
		}
		else if(order.number && order.number.length !== 9) {
			swal.fire('Nomor PR harus 9 digit', '', 'info')
		}
		else {
			this.setState({loading: true});
			
			order.approval0 = true
			apiCall.put(`/orders/${this.state.id}`, {
				formData: order,
			})
			.then(res => {
				this.setState({loading: false});
				swal.fire('Sukses mengupdate order', '', 'success')
				this.fetchOrder()
			})
			.catch(err => {
				if (err.response) {
					swal.fire(err.response.data.error, '', 'error')
					console.log(err.response.data.error)
				} else {
					swal.fire('Terjadi kesalahan pada sistem', '', 'error')
				}
				this.setState({loading: false});
			})
		}
	}

	handleSaveNoPR = () => {
		let {order} = this.state
		if (order.number === null || order.number === '') {
			swal.fire('Informasi', 'Masukkan No PR', 'info')
		} else if(order.number && order.number.length !== 9) {
			swal.fire('Informasi', 'Nomor PR harus 9 digit', 'info')
		} else {
			swal.fire({
				  title: 'Anda Yakin?',
				  text: "Menyimpan No PR ?",
				  type: 'info',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya, simpan!'
				}).then((result) => {
				  if (result.value) {
				    this.setState({loading: true});
						apiCall.put(`/orders/simpan-pr/${this.state.id}`, {
							orderNumber: order.number
						})
						.then(res => {
							this.setState({loading: false, sttNoPr: order.number});
							swal.fire('Sukses menyimpan No PR', '', 'success')
							this.fetchOrder()
						})
						.catch(err => {
							if (err.response) {
								swal.fire(err.response.data.error, '', 'warning')
								console.log(err.response.data.error)
							} else {
								swal.fire('Terjadi kesalahan pada sistem', '', 'warning')
							}
							this.setState({loading: false});
						})
				  }
				})
		}
	}

	handleUpadateQty = () => {
		let {order, fixItems, logChangeQty} = this.state
		if (this.state.logChangeQty.length > 0) {
			if(this.sumValue('quantity') > 50000000 || this.sumValue('quantitySupply') > 50000000) {
				swal.fire('Batas Pemesanan', 'Total tidak boleh lebih dari Rp 50.000.000', 'info')
			}
			else {
				swal.fire({
				  title: 'Anda Yakin?',
				  text: "Anda akan mengubah quantity item order",
				  type: 'info',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Ya, ubah!'
				}).then((result) => {
				  if (result.value) {
				    this.setState({loading: true});
						apiCall.put(`/orders/qty/${this.state.id}`, {
							formData: order,
							fixItems: fixItems,
							logChangeQty: logChangeQty
						})
						.then(res => {
							this.setState({loading: false});
							swal.fire('Sukses mengupdate quantity order', '', 'success')
							this.fetchOrder()
						})
						.catch(err => {
							if (err.response) {
								swal.fire(err.response.data.error, '', 'warning')
								console.log(err.response.data.error)
							} else {
								swal.fire('Terjadi kesalahan pada sistem', '', 'warning')
							}
							this.setState({loading: false});
						})
				  }
				})
			}
		}
	}

	cancelReject = (item) => {
		swal.fire({
		  title: 'Anda Yakin?',
		  text: "Anda akan membatalkan reject order ?",
		  type: 'info',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, Batalkan Reject!'
		}).then((result) => {
		  if (result.value) {
				apiCall.put(`/orders/${this.state.id}/reject/cancel`, {
					items: item
				})
				.then(res => {
					this.setState({loading: false});
					swal.fire(`Berhasil Membatalkan Reject`, '', 'success')
					this.fetchOrder()
				})
				.catch(err => {
					if (err.response) {
						swal.fire(err.response.data.message, '', 'error')
						console.log(err.response.data.error)
					} else {
						swal.fire('Terjadi kesalahan pada sistem', '', 'error')
					}
					this.setState({loading: false});
				})
		  }
		})
	}

	cancelApprove = (item) => {
		let items = {}
		if (item) {
			items= item
		} else {
			items= {
				approval: 'approval0'
			}
		}
		swal.fire({
		  title: 'Anda Yakin?',
		  text: "Anda akan membatalkan approval ?",
		  type: 'info',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, Batalkan Approve'
		}).then((result) => {
		  if (result.value) {
				apiCall.put(`/orders/${this.state.id}/approve/cancel`, {
					items: items
				})
				.then(res => {
					this.setState({loading: false});
					swal.fire(`Berhasil Membatalkan Approve`, '', 'success')
					this.fetchOrder()
				})
				.catch(err => {
					if (err.response) {
						swal.fire(err.response.data.message, '', 'error')
						console.log(err.response.data.error)
					} else {
						swal.fire('Terjadi kesalahan pada sistem', '', 'error')
					}
					this.setState({loading: false});
				})
		  }
		})
	}


	handleApprovalSubmit = (item, index) => {
		swal.fire({
		  title: 'Anda Yakin?',
		  text: "Anda akan approve order ?",
		  type: 'info',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, approve!'
		}).then((result) => {
		  if (result.value) {
				apiCall.put(`/orders/${this.state.id}/approve`, {
					items: item
				})
				.then(res => {
					this.setState({loading: false});
					swal.fire(`Berhasil Approve`, '', 'success')
					this.fetchOrder()
				})
				.catch(err => {
					if (err.response) {
						swal.fire(err.response.data.message, '', 'error')
						console.log(err.response.data.error)
					} else {
						swal.fire('Terjadi kesalahan pada sistem', '', 'error')
					}
					this.setState({loading: false});
				})
		  }
		})
	}

	handleRejectSubmit = (approvalItem, index) => {
		swal.fire({
		  title: 'Reject Order',
		  html: 'Masukkan Alasan Reject Order',
		  input: 'textarea',
		  inputAttributes: {
		    autocapitalize: 'off'
		  },
		  showCancelButton: true,
		  confirmButtonText: 'Reject',
		  showLoaderOnConfirm: true,
		  preConfirm: (value) => {
				return apiCall.post(`/orders/${this.state.id}/reject`, {
					items: approvalItem,
					notes: value,
					index: auth.getRole() === 2 ? index : index + 1
				})
				.then(res => {
					return res.data.data
				})
				.catch(err => {
					swal.fire(`Gagal Reject Order`, '', 'warning')
				})
		  },
		  allowOutsideClick: () => !swal.isLoading()
		}).then((result) => {
		  if (result.value) {
		  	this.fetchOrder()
		    swal.fire(`Sukses Reject Order`, '', 'info')
		  }
		})
	}

	renderApprovalButton = (item, index) => {
		const {order} = this.state
		const user = auth.getUser()
		if ( order[item.approvealBefore] === true && order[item.approval] === null && order[item.approvalBy] && (order[item.approvalBy]._id === user._id)) {
				return (
					<div>
						<button style={{marginRight: 5}} onClick={() => this.handleRejectSubmit(item, index)} className="button is-small is-rounded">
							Reject
						</button>
						<button onClick={() => this.handleApprovalSubmit(item, index)} className="button is-small is-rounded is-primary">
							Approve
						</button>
					</div>
				)
		}
		else if (order[item.approval] === true) {
			const signatureImage = order[`approval${index + 1}By`] ? order[`approval${index + 1}By`]['signatureImage'] : 'approvedDefault.png'
			return (
				order[`approval${index + 1}By`] &&
				<img src={`${BASE_URL}/img/${signatureImage}`} alt="ttd" style={{height: 80}}/>	
			)
		}
		
		return 
	}

	renderApprovalButtonSP = (item, index) => {
		const {order} = this.state
		let role = item.role
		if (role === auth.getRole() && order[item.approval] === null) {
			if (order['approval5'] === true) {
				console.log('NOT APPROVE SP')
				return (
					<div>
						<button style={{marginRight: 5}} onClick={() => this.handleRejectSubmit(item, index)} className="button is-small is-rounded">
							Reject
						</button>
						<button onClick={() => this.handleApprovalSubmit(item, index)} className="button is-small is-rounded is-primary">
							Approve
						</button>
					</div>
				)
			}
		}
		else if (order[item.approval] === true) {
			const signatureImage = order[`${item.approval}By`] ? order[`${item.approval}By`]['signatureImage'] : 'approvedDefault.png'
			return (
				order[`${item.approval}By`] &&
				<img src={`${BASE_URL}/img/${signatureImage}`} alt="ttd" style={{height: 80}}/>
			)
		}
		
	}



	renderVendorApprovalButton = (item, index) => {
		const {order} = this.state

		if (item.role === auth.getRole() && order[item.approval] === null && order['approval3'] !== null) {
			if (authorize.belongs(order[`approval4By`]['_id'], '_id')) {
				return (
					<div style={{marginTop: 10}}>
						<button style={{marginRight: 5}} onClick={() => this.handleRejectSubmit(item, index)} className="button is-small is-rounded">
							Reject
						</button>
						<button onClick={() => this.handleVendorApprovalSubmit(item)} className="button is-small is-rounded is-primary">
							Approve
						</button>
					</div>
				)
			}
		}
		else if (order[item.approval] === true) {
			const signatureImage = order['approval4By'] ? order['approval4By']['signatureImage'] : 'approvedDefault.png'
			return (
				<img src={`${BASE_URL}/img/${signatureImage}`} alt="ttd" style={{height: 80}}/>	
			)
		}
		
		return 
	}

	handleVendorApprovalSubmit = (item) => {
		let {order} = this.state

		let qtySupplyEmpty = order.items.find(x => x.quantitySupply === undefined || x.quantitySupply === '')
		let qtySupplyBigger = order.items.find(x => Number(x.quantitySupply) > Number(x.quantity))

		if (qtySupplyEmpty) {
			return swal.fire('Input Quantity Supply yang masih kosong', '', 'error')
		}
		if (qtySupplyBigger) {
			return swal.fire('Quantity Supply tidak boleh lebih besar dari Quantity Demand', '', 'error')
		}
		else if(this.sumValue('quantity') > 50000000 || this.sumValue('quantitySupply') > 50000000) {
			swal.fire('Melebihi Rp 50.000.000', 'Total tidak boleh lebih dari Rp 50.000.000', 'error')
		}
		else {
			apiCall.post(`/orders/${this.state.id}/approve-vendor`, {
				approvalItem: item,
				formData: order,
				notes: this.state.notes
			})
			.then(res => {
				this.setState({loading: false});
				swal.fire(`Berhasil Approve`, '', 'success')
				this.fetchOrder()
			})
			.catch(err => {
				if (err.response) {
					if (err.response.data.status === 201) {
						swal.fire(err.response.data.message, '', 'success')
					} else {
						swal.fire(err.response.data.message, '', 'error')
					}
				} else {
					swal.fire('Terjadi kesalahan pada sistem', '', 'error')
				}
				this.setState({loading: false});
			})
		}
	}

	renderRejectChangeButton = () => {
		const {order} = this.state

		let approvals = [
			{
				name: 'PIC Analyst',
				approval: 'approval0',
				role: 2
			},
			{
				name: 'Section Head Inventory Control',
				approval: 'approval1',
				role: 4
			},
			{
				name: 'Manajer Procurement',
				approval: 'approval2',
				role: 5
			},
			{
				name: 'Section Head Purchasing',
				approval: 'approval3',
				role: 6
			}
		]

		if (order.stockType === 'nonstock') {
			approvals = [
				{
					name: 'Section Head Bagian',
					approval: 'approval1',
					role: 7
				},
				{
					name: 'Manajer Fungsi',
					approval: 'approval2',
					role: 8
				},
				{
					name: 'Section Head Purchasing',
					approval: 'approval3',
					role: 6
				}
			]
		}

		for(let item of approvals) {
			if (order[item.approval] === null) {
				return true
			}
		}

		return false
	}

	sumValue = (qty) => {
		let sum = 0
		if (this.state.order.items && this.state.order.items.length > 0) {
			for(let item of this.state.order.items) {
				sum += (item.product.price * item[qty])
			}
		}

		return sum
	}

	print = () => {
		this.setState({printNote: true});
		// window.location.reload()
		setTimeout(function() {
			window.print()
    }, 1000);
		
		setTimeout(() => this.setFalse(), 1000);
	}

	setFalse = () => {
		this.setState({printNote: false});
	}

	isIdInOrder = () => {
		const {order} = this.state
		let arr = [order.createdBy._id, order.approval0By, order.approval1By ? order.approval1By._id : false, order.approval2By ? order.approval2By._id: false, order.approval3By._id]
		
		return arr.includes(auth.getUser()._id)
	}

	cekApprovelUser = async () => {
		const {order} = this.state
		const user = auth.getUser()
		const role = auth.getRole()
		let status = false
		if ((role === 2 && order.stockType === 'stock') && (order.approval0 === null || order.approval0 === false) && !order.approval5By && (!order.rejected || order.rejected === null)) {
			status = true
		} else if (role === 3 && (order.approval0 === null || order.approval0 === false) && !order.approval5By && (!order.rejected || order.rejected === null)) {
			status = true
		} else if ((role === 4 || role === 7) && (order.approval1 === null || order.approval1 === false) && !order.approval5By && user._id === order.approval1By._id && (!order.rejected || order.rejected === null)) {
			status = true
		} else if ((role === 5 || role === 8) && (order.approval2 === null || order.approval2 === false) && !order.approval5By && user._id === order.approval2By._id && (!order.rejected || order.rejected === null)) {
			status = true
		} else if (role === 6 && (order.approval3 === null || order.approval3 === false) && user._id === order.approval3By._id && (!order.rejected || order.rejected === null)) {
			status = true
		} else if ((role === 10 || role === 11 || role === 12) && (order.approval5 === null || order.approval5 === false) && order.approval5By && (!order.rejected || order.rejected === null)) {
				status = true
		} else {
			status = false
		}
		this.setState({updateQty: status})
	}

	render() {
		const {order, orderNumber} = this.state
		let role = auth.getRole()

		let approvals = [
			{
				name: 'Section Head Inventory Control',
				approval: 'approval1',
				approvalBy: 'approval1By',
				approvealBefore: 'approval0',
				approvealAftar: 'approval2',
				approvealAftarBy: 'approval2By',
				role: 4
			},
			{
				name: 'Manajer Procurement',
				approval: 'approval2',
				approvalBy: 'approval2By',
				approvealBefore: 'approval1',
				approvealAftar: 'approval3',
				approvealAftarBy: 'approval3By',
				role: 5
			},
			{
				name: 'Section Head Purchasing',
				approval: 'approval3',
				approvalBy: 'approval3By',
				approvealBefore: 'approval2',
				approvealAftar: 'approval4',
				approvealAftarBy: 'approval4By',
				role: 6
			}
		]

		if (order.stockType === 'nonstock') {
			approvals = [
				{
					name: 'Section Head Bagian',
					approval: 'approval1',
					approvalBy: 'approval1By',
					approvealBefore: 'approval0',
					approvealAftar: 'approval2',
					approvealAftarBy: 'approval2By',
					role: 7
				},
				{
					name: 'Manajer Fungsi',
					approval: 'approval2',
					approvalBy: 'approval2By',
					approvealBefore: 'approval1',
					approvealAftar: 'approval3',
					approvealAftarBy: 'approval3By',
					role: 8
				},
				{
					name: 'Section Head Purchasing',
					approval: 'approval3',
					approvalBy: 'approval3By',
					approvealBefore: 'approval2',
					approvealAftar: 'approval4',
					approvealAftarBy: 'approval4By',
					role: 6
				}
			]
		}

		if (order.approval5By) {
			approvals = [
				{
					name: order.approval5By.jabatan,
					approval: 'approval5',
					approvalBy: 'approval5By',
					approvealAftarBy: 'approval3By',
					approvealAftar: 'approval3',
					role: order.approval5By.role
				},
				{
					name: 'Section Head Purchasing',
					approval: 'approval3',
					approvalBy: 'approval3By',
					approvealBefore: 'approval5',
					approvealAftar: 'approval4',
					approvealAftarBy: 'approval4By',
					role: 6
				}
			]
		}

		let vendorname = ''
		if (order.approval4By) {
			if (order.approval4By.vendor) {
				vendorname = order.approval4By.vendor.name
			}
		}

		return (
			<div>
				<section className="section">
					<div className="container">
						
						<div className="columns">
							<div className="column">
								<div className="card">
									<div className="card-content">
										<div className="content is-small">
											{
												order.approval4 === true &&
												<div className="is-pulled-right">
													<button className="button is-rounded " onClick={this.print}>
														<span className="icon">
															<i className="fa fa-print"></i>
														</span>
													</button>
												</div>
											}
											<h6 className="title is-6">NOTA PENGAMBILAN BARANG</h6>

											<table className="table-no-border">
												<tbody>
													<tr>
														<th>No. Registrasi</th>
														<td>: </td>
														<td>{order.registrationNumber}</td>
													</tr>
													<tr>
														<th>No. PR</th>
														<td>: </td>
														<td>
															{
																((order.createdBy && order.approval4 === null && orderNumber === false &&
																	(
																		(order.createdBy && auth.getRole() === 3 && auth.getUser()._id === order.createdBy._id && order.stockType === 'nonstock') || 
																		(auth.getRole() === 2 && order.stockType === 'stock' && order.approval0 === null && !order.approval5By) ||
																		(order.createdBy && (auth.getRole() === 10 || auth.getRole() === 11 || auth.getRole() === 12) && order.approval5 === null && auth.getUser()._id === order.createdBy._id && order.approval5By)
																	)
																) || (orderNumber === false && order.typeOrder === 2 && auth.getUser()._id === order.createdBy._id)) ?
																	<input type="text" name="number" onChange={this.handleInputChange} value={order.number ? order.number: ''} />
																:
																	order.number
															}
														</td>
													</tr>
													<tr>
														<th>TANGGAL</th>
														<td>: </td>
														<td>
															{moment(order.createdAt).format('DD/MM/YYYY')}
														</td>
													</tr>
													<tr>
														<th>DARI</th>
														<td>: </td>
														<td>PT PERTAMINA (PERSERO) RU IV CILACAP</td>
													</tr>
													<tr>
														<th>KEPADA</th>
														<td>: </td>
														<td>{vendorname}</td>
													</tr>
												</tbody>
											</table>
											<p>Sesuai dengan Surat Perjanjian Direct Buying antara PT Pertamina RU IV dengan <u>{vendorname}</u>,  harap dapat memberikan barang seperti di bawah ini:</p>

											<table className="table is-narrow is-bordered">
												<thead>
													<tr>
														<th>No.</th>
														<th>Kode Barang</th>
														<th>Deskripsi Barang</th>
														<th>Unit Price</th>
														<th>Qty. Demand</th>
														<th>Subtotal</th>
														<th>Qty. Supply</th>
														<th>Subtotal</th>
														<th>UoM</th>
													</tr>
												</thead>
												<tbody>
													{
														order.items && order.items.length > 0 && order.items.map((item, index) => (
															<tr key={index}>
																<td>{index + 1}</td>
																<td>{item.product.code}</td>
																<td>{item.product.name}</td>
																<td>{numeral(item.product.price).format('$ 0,0')}</td>
																<td>
																	{
																		this.state.updateQty === true ?
																			<input type="number" name="quantity" value={item.quantity} onChange={(e) => this.handleQtyChange(e, item.product)} />
																		:
																			item.quantity																			
																	}
																</td>
																<td>{numeral(item.product.price * item.quantity).format('$ 0,0')}</td>
																<td>
																	{
																		((auth.getRole() === 1 && (order.approval4 === true || order.approval4 === false)) || auth.getRole() !== 1) ?
																			item.quantitySupply
																		:
																			<input type="number" name="quantitySupply" value={item.quantitySupply ? item.quantitySupply : ''} onChange={(e) => this.handleQtyChange(e, item.product)} />
																	}
																</td>
																<td>{numeral(item.product.price * item.quantitySupply).format('$ 0,0')}</td>
																<td></td>
															</tr>
														))
													}
													<tr>
														<th colSpan="5"></th>
														<th>{numeral(this.sumValue('quantity')).format('$ 0,0')}</th>
														<th></th>
														<th>{numeral(this.sumValue('quantitySupply')).format('$ 0,0')}</th>
														<th></th>
													</tr>
												</tbody>
											</table>

											<div className="columns is-mobile has-text-centered">
												{
													approvals.map((item, index) => (
														<div className="column" key={index}>
															<div className="card">
																<div className="card-content">
																	{item.name}
																	<br/>
																	{
																		(index === 0 && order.stockType === 'nonstock' && order.approval5By) && 
																		''
																	}
																	{
																		(index === 0 && order.stockType === 'nonstock' && !order.approval5By) && 
																		<label>{(order.createdBy && order.createdBy.bagian) && order.createdBy.bagian.name}</label>
																	}
																	{
																		(index === 1 && order.stockType === 'nonstock' && !order.approval5By) && 
																		<label>{(order.createdBy && order.createdBy.fungsi) && order.createdBy.fungsi.name}</label>
																	}
																	<br/>
																	<br/>
																	{
																		order.approval5By ?
																		this.renderApprovalButtonSP(item, index)
																		:
																		this.renderApprovalButton(item, index)
																	}
																	<br/>
																	( {order[item.approvalBy] ? order[item.approvalBy].name : '___________________'} )
																	{
																	  	order[item.approvalBy] && auth.getUser()._id === order[item.approvalBy]._id && order[item.approval] === true && order[item.approvealAftar] === null &&
																	  	<div style={{marginTop: 10}}>
																	  		<button onClick={() => this.cancelApprove(item)} className="button is-primary is-rounded is-small">Batal Approve</button>
																	  	</div>
																	 }
																</div>
															</div>
															{															
																(index  === order.rejected - 1) 
																&&
																<div className="box has-background-warning">
																	<div className="field">
																	  <label className="label" style={{color: 'blue'}}>Catatan Reject</label>
																	  <hr />
																	  <div className="control">
																	    <p style={{color: 'black'}}>
																	    	{order.notes}
																	    </p>
																	  </div>
																	  <hr />
																	  {
																	  	order[item.approvalBy] && auth.getUser()._id === order[item.approvalBy]._id &&
																	  	<div style={{alignItems: 'right'}}>
																	  		<button onClick={() => this.cancelReject(item)} className="button is-primary is-rounded is-label">Batal Reject</button>
																	  	</div>
																	  }
																	</div>
																</div>
															}
														</div>
													))
												}
											</div>

											<div className="columns is-mobile">
												<div className="column is-4">
													<div className="card">
														<div className="card-content">
															Keterangan:
															<br/>
															<br/>
															PIC    : {order.createdBy ? order.createdBy.name : ''}
															<br/>
															<br/>
															Bagian : {' '}
															{(order.createdBy && order.approval5By) && order.createdBy.jabatan} 
															{(order.createdBy && !order.approval5By && order.createdBy.bagian) && order.createdBy.bagian.name}
															<br/>
															<div style={{height: 50}}></div>
														</div>
													</div>
												</div>
												<div className="column is-4 has-text-centered">
													<div className="card">
														<div className="card-content">
															Menerima,
															<br/>
															Receiving - Warehousing,
															<br/>
															<div style={{height: 85}}></div>
															( ___________________________ )
														</div>
													</div>
												</div>
												<div className="column">
													<div className="card">
														<div className="card-content has-text-centered">
															Penyedia Barang,
															<br/>
															{vendorname}
															<br/>
															{this.renderVendorApprovalButton({name: 'Vendor', approval: 'approval4', role: 1}, 3)}
															<br/>
															( {order['approval4By'] ? order['approval4By'].name : '___________________'} )
														</div>
													</div>
													{															
														(order.rejected && order.rejected === 4) &&
														<div className="box has-background-warning">
															<div className="field">
															  <label className="label">Catatan Vendor</label>
															  <div className="control">
															    <p>
															    	{order.notes}
															    </p>
															  </div>
															</div>
															{
														  	(role === 1 && order.approval4 === false) &&
														  	<div style={{alignItems: 'right', marginTop: 10}}>
														  		<button onClick={() => this.cancelReject({approval: 'approval4'})} className="button is-primary is-rounded is-small">Batal Reject</button>
														  	</div>
															}
														</div>
													}
												</div>
											</div>

											<div className="columns is-mobile">
												<div className="column is-8">
													<p>
														<i><b>- Maksimal pengiriman 2 hari kerja setelah Nota Pengambilan Barang release dari Pertamina RU IV Cilacap</b></i>
														<br/>
														<i><b>- Nota Pengambilan barang ini merupakan bagian yang tidak terpisahkan dari dokumen pembelian (PO)</b></i>
														<br/>
														<i><b>- Nota Pengambilan Barang berlaku juga sebagai ijin masuk barang dari pihak ke - 3 ke Pertamina RU IV</b></i>
													</p>
												</div>
												<div className="column">
													<div className="card">
														<div className="card-content has-text-centered">
															{order.stockType === 'stock' ? 'Stock' : 'Non Stock'}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="columns">
							<div className="column has-text-right">								
								{
									(
										(role === 2 && order.approval0 === true && order.stockType === 'stock' && !order.approval5By && order.approval1 === null) ||
										(role === 3 && order.approval0 === true && order.stockType === 'nonstock' && !order.approval5By && order.approval1 === null)
										) &&
									<div style={{width: 200, float: 'left'}}>
										<button onClick={() => this.cancelApprove()} className="button is-primary is-rounded">Batalkan Approve</button>
									</div>
								}
								{
									role === 2 && order.approval0 === null && order.stockType === 'stock' && !order.approval5By &&
									<div style={{width: 200, float: 'left'}}>
										<button style={{marginRight: 5}} onClick={() => this.handleRejectSubmit({approval: 'approval0'}, 0)} className="button is-rounded">
											Reject
										</button>
										<button onClick={this.handleSaveNoPR} className="button is-primary is-rounded">Approve</button>
									</div>
								}
								{
									role === 3 && order.approval0 === null && order.stockType === 'nonstock' && this.state.sttNoPr === null &&
									<button 
										onClick={this.handleSaveNoPR} 
										className={'button is-link is-rounded'}>
											Simpan No PR
										</button>
								}
								{
									(orderNumber === false && order.typeOrder === 2 && auth.getUser()._id === order.createdBy._id) &&
									<button 
									onClick={this.handleSaveNoPR} 
									className={'button is-link is-rounded'}>
										Simpan No PR
									</button>
								}
								{
									(role === 10 || role === 11 || role === 12) && order.approval5 === null && order.approval5By &&
									<button 
										onClick={this.handleSaveNoPR} 
										className={'button is-link is-rounded'}>
											Simpan No PR
										</button>
								}
								{' '}
								{
									this.state.updateQty === true && 
									<button 
										onClick={this.handleUpadateQty} 
										className={this.state.logChangeQty.length === 0 ? 'button is-light' : 'button is-link is-rounded'}>
											Simpan Perubahan Quantity
										</button>
								}
								{															
									order.rejected  === 0 &&
									<div className="box has-background-warning">
										<div className="field">
										  <label className="label">Catatan Reject PIC Analyst</label>
										  <div className="control">
										    <p>
										    	{order.notes}
										    </p>
										  </div>
										</div>
									</div>
								}
								{
							  	(role === 2 && order.stockType === "stock" && order.approval0 === false) &&
							  	<div style={{alignItems: 'right'}}>
							  		<button onClick={() => this.cancelReject({approval: 'approval0'})} className="button is-primary is-rounded is-label">Batal Reject</button>
							  	</div>
								}
							</div>
						</div>
					</div>
					<Loading load={this.state.load} />
				</section>
				<p className={`content is-small`}>
					<i><b>(Nota Pengambilan Barang ini dinyatakan sah sebagai Izin Masuk Barang (IMB), dicetak dari sistem e-Direct Buying Pertamina RU IV Cilacap)</b></i>
				</p>
			</div>
		);
	}
}

const mapStateToProps = ({cart}) => {
  return {cart}
}

export default connect(mapStateToProps, actions)(OrderDetail)
