import React, { Component } from 'react';
import swal from 'sweetalert2'
import {Link} from 'react-router-dom'
import ReactTable from 'react-table'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'
import './stylesOrder.css'
import "react-table/react-table.css";

class Order extends Component {
	state = {
		orders: [],
		tabActive: 1,
		load: true,
		page: null,
		currentPage: 1,
		pages: null,
		perPage: 10,
		typeOrder: '0',
		pageSize: 5
	}

	componentDidMount() {
		if (!auth.isAuth()) 
			this.props.history.push('/login')
		else
			if(auth.getRole() === 1){
				this.props.history.push('/')
			}
	}

	fetchOrder = (state, instance) => {
		const { page, pageSize, filtered, sorted, typeOrder } = state ? state : this.state;
		const filterString = JSON.stringify(filtered);
    let params = `?page=${page}&pageSize=${pageSize}&filtered=${filterString}&typeOrder=${typeOrder}`
    const paramsEncode = encodeURI(params)
		apiCall.get(`/orders-news${paramsEncode}`)
		.then(res => {
			const nilai = Math.ceil(res.data.totalRow / pageSize, 5)
			console.log(res, nilai)
			this.setState({
				orders: res.data.data,
				currentPage: page,
        perPage: pageSize,
        pages: Math.ceil(res.data.totalRow / pageSize, 5),
				load: false,
				page, pageSize, filtered, sorted
			})
		})
		.catch(err => {
			this.setState({load: false})
		})
	}

	renderStatus = (order) => {
		let statuses = {
			stock: [
				'PIC Analyst', 'SH Inventory Control', 'Mgr. Procurement', 'SH. Purchasing', 'Vendor'
			],
			nonstock: [
				'PIC Bagian', 'SH Bagian', 'Mgr. Fungsi', 'SH. Purchasing', 'Vendor'
			]
		}

		let view = []
		for(let i=0; i< statuses[order.stockType].length; i++) {
			view.push(
				<li key={i}>
					{statuses[order.stockType][i]} : {order[`approval${i}`] === true ? <span className="tag is-success">Approved</span> : (order[`approval${i}`] === false ? <span className="tag is-danger">Rejected</span> : '')}
				</li>
			)
		}

		return view
	}



	deleteOrder = (id) => {
		swal.fire({
		  title: 'Yakin?',
		  text: "Data yang dihapus tidak bisa dikembalikan",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#d33',
		  cancelButtonColor: '#3085d6',
		  confirmButtonText: 'Hapus'
		}).then((result) => {
		  if (result.value) {
		  	apiCall.delete(`/orders/${id}`)
		  	.then(res => {
		  		swal.fire(
		  		  'Berhasil!',
		  		  'Data terhapus',
		  		  'success'
		  		)

					this.fetchOrder()
		  	})
		  	.catch(err => {
		  		if (err.response) {
		  			swal.fire(err.response.data.message, '', 'error')
		  			console.log(err.response.data.error)
		  		} else {
		  			swal.fire('Terjadi kesalahan pada sistem', '', 'error')
		  		}
		  	})
		  }
		})
	}

	handleInputChangeOrder = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;
		this.setState({
			[name]: value,
			page: 0,
			pageSize: 5
		}, () => {
			this.fetchOrder()
		})	
	}

	customFilter = ({filter, onChange }) => {
    return (
      <select
        onChange={event => onChange(event.target.value)}
        style={{ width: "100%" }}
        value={filter ? filter.value : "all"}
      > 
        <option value="all">Tampil Semua</option>
        <option value={auth.getRole()}>Butuh Approval</option>
      </select>
    );
  }


	render() {
		const {pages, load, typeOrder} = this.state
		const columns = [{
	    Header: 'No',
	    filterable: false,
	    minWidth: 75,
	    Cell: props => <span>{props.index + 1}</span>
	  }, {
	    Header: 'No PR',
	    accessor: 'number',
	    minWidth: 85
	  }, {
	    Header: 'No Registrasi',
	    accessor: 'registrationNumber',
	    minWidth: 150
	  }, {
	    Header: 'PIC',
	    accessor: 'createdBy.name',
	    minWidth: 200
	  }, {
	    Header: 'Item',
	    accessor: 'items',
	    filterable: false,
	    minWidth: 300,
	    Cell: props => (
	    		<div className="content is-small">
						<table className="table is-narrow">
							<thead>
								<tr>
									<th>No.</th>
									<th>Barang</th>
									<th>Qty Demand</th>
									<th>Qty Supply</th>
								</tr>
							</thead>
							<tbody>
								{
									props.original.items.length > 0 &&
									props.original.items.map((item2, index2) => (
										<tr key={index2}>
											<td>{index2 + 1}</td>
											<td>{item2.product.name}</td>
											<td>{item2.quantity}</td>
											<td>{item2.quantitySupply}</td>
										</tr>
									))
								}
							</tbody>
						</table>
					</div>
	    	)
	  },
	  {
			    Header: 'Status',
			    accessor: 'approved',
			    filterMethod: (filter, row) => {
        		return row[filter.id] === filter.value;
      		},
	    		Filter: ({ filter, onChange }) => this.customFilter({filter, onChange }),
			    minWidth: 200,
			    Cell: props => (
			    		<div className="content is-small">
								{
									props.original.approval5By ?
										<div>
										<ol style={{marginLeft: 10}}>
											<li>
												{props.original.approval5By.jabatan} : {props.original[`approval5`] === true ? <span className="tag is-success">Approved</span> : (props.original[`approval5`] === false ? <span className="tag is-danger">Rejected</span> : '')}
											</li>
											<li>
												SH. Purchasing : {props.original[`approval3`] === true ? <span className="tag is-success">Approved</span> : (props.original[`approval3`] === false ? <span className="tag is-danger">Rejected</span> : '')}
											</li>
											<li>
												Vendor : {props.original[`approval4`] === true ? <span className="tag is-success">Approved</span> : (props.original[`approval4`] === false ? <span className="tag is-danger">Rejected</span> : '')}
											</li>
										</ol>
										{
											props.original.statusOrder &&
												<label><b>Note.</b> {props.original.statusOrder}</label>
										}
										</div>
									:
									<div>
										<ol style={{marginLeft: 10}}>
											{this.renderStatus(props.original)}																		
										</ol>
										{
											props.original.statusOrder &&
											<label><b>Note.</b> {props.original.statusOrder}</label>
										}
									</div>
								}
						</div>
			    	)
			  }, 
        {
			    Header: 'Aksi',
			    filterable: false,
			    minWidth: 200,
			    fixed: "right",
			    Cell: props => (
		           <div>
		           <center>
		               <Link 
		               		target="_blank"
											to={`/orders/${props.original._id}`}
											className="button is-outlined is-small is-rounded"
										>
											<span className="icon">
											  <i className="fas fa-pencil-alt"></i>
											</span>
											&nbsp;
											Detail
										</Link>
										{' '}
										{
												props.original.createdBy && props.original.createdBy._id === auth.getUser()._id &&
												<button 
				               		onClick={() => this.deleteOrder(props.original._id)}
													className="button is-outlined is-small is-danger is-rounded"
												>
													<span className="icon">
													  <i className="fas fa-pencil-alt"></i>
													</span>
													&nbsp;
													Drop
												</button>
									}
		            </center>
		           </div>
		       )
			  }]
		return (
			<section className="section">
				<div className="container-fluid">
					<div className="columns">
						<div className="column is-4">
							<div className="field">
							  <label className="label">Filter</label>
							  <div className="control">
							    <select 
							    	className="input"
							    	name="typeOrder" 
							    	onChange={this.handleInputChangeOrder} 
							    	value={typeOrder ? typeOrder : ''}
							    >
							    	<option value="0">Semua Order</option>
							    	<option value="1">Order Saya</option>
							    </select>
							  </div>
							</div>
						</div>
					</div>
						<div className="columns">
							<div className="column" style={{overflow: 'hidden'}}>
								<ReactTable
									manual
							    data={this.state.orders}
							    columns={columns}
							    filterable
							    pages={pages}
							    loading={load}
				          defaultPageSize={5}
				          onFetchData={this.fetchOrder}
							  />
				  	</div>
				  </div>
				</div>
			</section>
		);
	}
}

export default Order
