import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import Pagination from 'rc-pagination'
import 'rc-pagination/assets/index.css'
import apiCall from '../../services/apiCall'
import queryString from '../../utils/queryString'
import Loading from '../../components/Loading'
import ProductList from '../../components/ProductList/ProductList'
import SearchBar from '../../components/SearchBar/SearchBar'

class Search extends Component {
	state = {
		data: [],
		keyword: '',
		page: 1,
		total: null,
    activePage: 1,
    load: false,
    modalFilter: false,
    userVendors: [],
    vendor: '',
    typeMat: ''
	}

	componentDidMount() {
    this.updateSearch(this.props)
    this.fetchVendors()
	}

	componentWillReceiveProps(nextProps) {
    this.updateSearch(nextProps)
	}

	setAvalableFilter = () => {
		this.setState({modalFilter: !this.state.modalFilter})
	}
	updateSearch = (props) => {
		let params = queryString.parse(props.location.search)
		console.log(params)
		const keyword = params.keyword ? params.keyword : ''
		const page = params.page ? params.page : 1
		const vendor = params.vendor ? params.vendor : ''
		const typeMat = params.type ?  params.type : ''
		this.setState({
			keyword,
			page: Number(page) || 1,
      activePage: Number(page) || 1,
      vendor,
      typeMat
		});

		this.fetchProducts(keyword, vendor, typeMat, page)
	}

	fetchProducts = async (keyword, vendor, stockType, page) => {
		this.setState({load: true})
		apiCall.get('/products', {
				params: {
					keyword,
					vendor,
					stockType,
					page
				}
			})
		.then((response) => {
			console.log(response, 'product')
			this.setState({
				data: response.data.data,
				total: response.data.total,
				load: false
			});
		})
		.catch((err) => {
			this.setState({load: false})
			console.log(err)
		})
	
	}

	fetchVendors = () => {
		apiCall.get(`/vendors`)
		.then(res => {
			this.setState({
				userVendors: res.data.data,
			})
		})
		.catch(err => {
			console.log(err.message)
		})
	}

	handleFilter = async () => {
		let queryS = queryString.parse(this.props.location.search)
		const {vendor, typeMat} = this.state
		let params = ''
		const page = `?page=1`
		const keyword = queryS.keyword ? `&keyword=${queryS.keyword}` : '' 
		params += page
		params += keyword
		if (vendor !== '') {
			params += `&vendor=${vendor}`
		}
		if (typeMat !== '') {
			params += `&type=${typeMat}`
		}
		this.setAvalableFilter()
		this.props.history.push(params)
	}

  handlePageChange = (page) => {
    this.setState({activePage: page})
    const {vendor, typeMat, keyword} = this.state
    let params = `?page=${page}`
    if (vendor !== '') {
			params += `&vendor=${vendor}`
		}
		if (typeMat !== '') {
			params += `&type=${typeMat}`
		}
		if (keyword !== '' && keyword !== undefined) {
			params += `&keyword=${keyword}`	
		}
    this.props.history.push(`/search${params}`)
  }

  handleInput = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}

	resetFilter = () => {
		this.setState({keyword: '', vendor: '', typeMat: ''})
		this.props.history.push(`/search?page=1`)	
	}

	render() {
		const {keyword, vendor, typeMat} = this.state
		return (
			<section className="section">
				<div className="container">
					<div className="columns">
						<div className="column is-8 is-offset-2">
							<SearchBar history={this.props.history} />
						</div>
					</div>
					<div className="columns">
						<div className="column is-8 is-offset-2">
							<div className="columns is-mobile">
								<div className="column">
									<div className="tabs is-small">
									  <ul>
									    <li className="is-active"><Link to="#">Produk</Link></li>
									  </ul>
									</div>
								</div>
								<div className="column">
									<div className="is-pulled-right">
										{
											(keyword !== '' || vendor !== '' || typeMat !=='') &&
											<button className="button is-small is-danger tooltip" onClick={() => this.resetFilter()} data-tooltip="Reset Filter">
												<span className="fa fa-reply"></span> Reset Filter
											</button>
										}
											{' '}
											<button className="button is-small is-info tooltip" onClick={() => this.setAvalableFilter()} data-tooltip="Filter Material">
												<span className="fa fa-filter"></span> Filter
											</button>
									</div>
								</div>
							</div>

							<ProductList 
								data={this.state.data} 
								total={this.state.total}
								keyword={this.state.keyword}
							/>
						</div>
						<div className="column">
							
						</div>
					</div>
          <hr />
          <div className="columns">
            <div className="column has-text-centered">
              {
                this.state.total > 0 &&
	              <Pagination 
	                current={this.state.activePage}
	                total={this.state.total}
	                defaultCurrent={this.state.activePage}
	                pageSize={8}
	                onChange={this.handlePageChange}
	                showLessItems={true}
	                 />
              }
              {
              	(this.state.total === 0 && this.state.load === false) &&
                <div className="columns">
									<div className="column">
										<i className="fas fa-shopping-cart" style={{width: 250, height: 250}}></i><br />
										<label style={{fontSize: 30}}>Tidak Ada Produk</label>
									</div>
								</div>
              }
            </div>
          </div>
				</div>
				<Loading load={this.state.load} />
				<div class={this.state.modalFilter ? 'modal is-active' : 'modal'}>
				  <div class="modal-background"></div>
				  <div class="modal-card">
				    <header class="modal-card-head">
				      <p class="modal-card-title">Filter Material</p>
				      <button class="delete" aria-label="close" onClick={() => this.setAvalableFilter()}></button>
				    </header>
				    <section class="modal-card-body">
				      <div class="field">
				      	<label class="label">Vendor</label>
							  <div class="control">
							    <div class="select is-info">
							      <select className="input" name="vendor" onChange={this.handleInput} value={this.state.vendor}>
							        <option value="">- Pilih Perusahaan -</option>
								    	{
								    		this.state.userVendors.map((item, index) => (
								    				<option key={index.toString()} value={`${item._id}`}>{item.vendor ? item.vendor.name : item.name}</option>		
								    			)
								    		)
								    	}
							      </select>
							    </div>
							  </div>
							</div>
							<div class="field">
				      	<label class="label">Type Material</label>
							  <div class="control">
							    <div class="select is-info">
							      <select className="input" name="typeMat" onChange={this.handleInput} value={this.state.typeMat}>
							      	<option value="">Pilih type material</option>
							        <option value="stock">Stock</option>
							        <option value="nonstock">Non Stock</option>
							      </select>
							    </div>
							  </div>
							</div>
				    </section>
				    <footer class="modal-card-foot">
				      <button class="button is-success" onClick={() => this.handleFilter()}>Terapkan</button>
				    </footer>
				  </div>
				</div>
			</section>
		);
	}
}

export default Search
