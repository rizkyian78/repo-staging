import React, { Component } from 'react';
import swal from 'sweetalert2'
import { Link } from 'react-router-dom'
import numeral from 'numeral'
import { connect } from 'react-redux'

import * as actions from '../../redux/actions'
import apiCall from '../../services/apiCall'
import authorize from '../../services/authorize'
import auth from '../../services/auth'
import { BASE_URL } from '../../constants'

class ProductDetailVerify extends Component {
	state = {
		id: null,
		data: {},
		selectedImage: null,
		edit: false,
		userVendors: [],
		vendor: ''
	}

	componentDidMount() {
		this.fetchProduct()
	}

	fetchProduct = async () => {
		const id = this.props.location.pathname.split('/')[2]
		this.setState({id})
		apiCall.get(`/products/${id}`)
		.then(res => {
			this.setState({
				data: res.data.data,
				vendor: res.data.data.vendor,
				selectedImage: res.data.data.featuredImage
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	handleInputChange = (event) => {
		const target = event.target;
		const value =  target.value;
		const name = target.name;

		let { data } = this.state

		data[name] = value
		this.setState({data})
	}

	selectImage = (image) => {
		this.setState({selectedImage: image});
	}

	renderActionButton = () => {
		const item = this.state.data

		switch(auth.getRole()){
			case 2:
				return (
					<button onClick={() => this.setState({edit: true})} className="button is-link is-medium is-rounded is-fullwidth">
						Verifikasi
					</button>
				)
			default:
				if (authorize.canCreateOrder()) {
					return (
						<button 
							className="button is-primary is-medium is-rounded is-fullwidth"
							onClick={() => this.props.addToCart(item)}
						>
							Add to Cart
						</button>
					)
				}
				
				return 
		}
	}

	handleSubmit = () => {
		const { data, id } = this.state

		apiCall.put(`/products/${id}`, {
			formData: data
		})
		.then(res => {
			swal.fire('Sukses edit produk', '', 'success')
			.then(() => {
				this.setState({edit: false});
				this.fetchProduct()
			})
		})
		.catch(err => {
			if (err.response) {
				swal.fire(err.response.data.message, '', 'error')
				console.log(err.response.data.error)
			} else {
				swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	render() {
		const { data, selectedImage } = this.state

		let vendorname = ''
		if (data.vendor) {
			if (data.vendor.vendor) {
				vendorname = data.vendor.vendor.name
			}
		}
		return (
			<section className="section">
				<div className="container">
					<div className="columns">
						<div className="column is-4 is-offset-2">
							<figure className="image is-square card">
							  {
		      	  		selectedImage ? 
		      	  			<img src={`${BASE_URL}/img/${selectedImage}`} alt="produk" style={{objectFit: 'contain'}}/>
		      	  		:
		      	  			<img src="/img/default_product.png" alt="product" style={{objectFit: 'cover'}}/>
		      	  	}
							</figure>
							<div className="columns is-gapless is-mobile">
								{
									data.images && data.images.map((item, index) => (
										<div className="column" key={index}>
											<div className="card">
												<Link to="#" onClick={() => this.selectImage(item)}>
													<figure className="image is-square">
													  {
													  	item ?
													  		<img src={`${BASE_URL}/img/${item}`} alt="produk" style={{objectFit: 'contain'}}/>
												  		:
												  			<img src="/img/default_product.png" alt="product" style={{objectFit: 'cover'}}/>
													  }
													</figure>
												</Link>
											</div>
										</div>
									))
								}
							</div>
						</div>
						<div className="column is-4">
							<h4 className="title is-4">
								{data.name}
							</h4>
							<b className="has-text-primary">
								{numeral(data.price).format('$ 0,0')}
							</b>
							<br/>
							Kode:&nbsp;
							{
								this.state.updated ?
									this.state.code
								: 
									data.code || <button className="button is-small is-rounded is-warning tooltip" data-tooltip="Menunggu Verifikasi">--</button>
							}
							<br/>
							<p style={{fontSize: 12, marginTop: 10}}>
								UoM: {data.uom}
								<br/>
								Brand: {data.brand}
								<br/>
								Vendor: {vendorname}
								<br/>
								Tipe: {data.stockType === 'nonstock' ? 'Non Stock (N)' : 'Stock (L)'}
							</p>
							<hr/>
							

							<b>Full Specification</b>
							{
								<p className="content is-small" style={{marginTop: 10}}>
									{data.specification && data.specification.split('\n').map((item2, key2) => {
									  return <span key={key2}>{item2}<br/></span>
									})}
								</p>
							}

							<button onClick={() => this.setState({edit: true})} className="button is-link is-medium is-rounded is-fullwidth">
								Verifikasi
							</button>
						</div>
					</div>
				</div>

				<div className={`modal ${this.state.edit && 'is-active'}`}>
				  <div className="modal-background" onClick={() => this.setState({edit: false})}></div>
				  <div className="modal-content">
				    <div className="box">
				    	<div className="content">
				    		<h5 className="title is-5">Verifikasi</h5>
				    		<div className="field">
				    		  <label className="label">Material Number / Kode Identifikasi</label>
		    		      <div className="control">
		    		        <input className="input" type="text" name="code" onChange={this.handleInputChange} value={data.code ? data.code : ''} />
		    		      </div>
				    		</div>

				    		<div className="field">
				    		  <label className="label">Tipe</label>
		    		      <div className="control">
		    		        <label className="radio">
		    		          <input 
		    		          	type="radio" 
		    		          	name="stockType" 
		    		          	value="stock" 
		    		          	onChange={this.handleInputChange} 
		    		          	checked={data.stockType === 'stock'}
		    		          />
		    		          &nbsp;
		    		          Stok
		    		        </label>
		    		        <label className="radio">
		    		          <input 
		    		          	type="radio" 
		    		          	name="stockType" 
		    		          	value="nonstock" 
		    		          	onChange={this.handleInputChange} 
		    		          	checked={data.stockType === 'nonstock'}
		    		          />
		    		          &nbsp;
		    		          Non Stok
		    		        </label>
		    		      </div>
				    		    
				    		</div>
				    	
				    		<div className="field is-grouped is-pulled-right">
				    			<div className="control">
				    			  <button onClick={() => this.setState({edit: false})} className="button is-text">Cancel</button>
				    			</div>
				    		  <div className="control">
				    		    <button className={`button is-link is-rounded`} onClick={this.handleSubmit}>Simpan</button>
				    		  </div>
				    		</div>

				    		<br/>
				    		<br/>

				    	</div>
				    </div>
				  </div>
				  <button className="modal-close is-large" aria-label="close" onClick={() => this.setState({edit: false})}></button>
				</div>
			</section>
		);
	}
}

export default connect(null, actions)(ProductDetailVerify)
