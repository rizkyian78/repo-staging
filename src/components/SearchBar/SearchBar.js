import React, { Component } from 'react';
import queryString from '../../utils/queryString'

class SearchBar extends Component {
	state = {
		keyword: '',
		keywordUri: '',
		vendor: '',
		type: ''
	}

	componentDidMount() {
		let params = queryString.parse(this.props.history.location.search)
		const keyword = params.keyword ? params.keyword : ''
		this.setState({keyword});
	}

	handleChange = (e) => {
		this.setState({keyword: e.target.value})
	}

	onSubmit = (e) => {
		let params = queryString.parse(this.props.history.location.search)
		const { keyword }  = this.state
		const page = `?page=1`
		const keywordUri = keyword ? `&keyword=${keyword}` : ''
		const vendor = params.vendor ? `&vendor=${params.vendor}` : ''
		const type = params.type ? `&type=${params.type}` : ''
		e.preventDefault()
		this.props.history.push(`/search${page}${keywordUri}${vendor}${type}`)
	}

	render() {
		const { size } = this.props

		return (
			<div>
				<form onSubmit={this.onSubmit}>
					<div className="field is-grouped">
					  <p className="control is-expanded">
					    <input 
					    	className={`input is-rounded ${size && 'is-' + size}`} 
					    	onChange={this.handleChange}
					    	type="text" 
					    	placeholder="Cari nama produk..."
					    	value={this.state.keyword ? this.state.keyword : ''}
					    />
					  </p>
					  <p className="control">
					    <button className={`button is-link is-rounded ${size && 'is-' + size}`}>
					      <span className="icon">
					        <i className="fas fa-search"></i>
					      </span>
					    </button>
					  </p>
					</div>
				</form>
			</div>
		);
	}
}

export default SearchBar
