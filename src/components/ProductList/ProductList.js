import React, { Component } from 'react';

import ProductCard from '../ProductCard/ProductCard'

class ProductList extends Component {
	render() {
		return (
			<div>
				{
					this.props.keyword &&
					<div className="content is-small" style={{marginBottom: 5}}>
						Menampilkan {this.props.total} produk untuk pencarian "<b>{this.props.keyword}</b>"
					</div>
				}

				<div className="columns is-multiline is-mobile">
					{
						this.props.data && this.props.data.length > 0 &&
						this.props.data.map((item, index) => (
							<div className="column is-6-mobile is-3-desktop" key={index} >
								<ProductCard item={item} />
							</div>
						))
					}
				</div>
			</div>
		);
	}
}

export default ProductList
