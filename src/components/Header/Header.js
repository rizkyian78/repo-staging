import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom'
import jwt from 'jsonwebtoken'
import { connect } from 'react-redux'

import auth from '../../services/auth'
import authorize from '../../services/authorize'
import './header.css'

const roles = [
	'',
	'Vendor',
	'PIC Analyst',
	'PIC Bagian',
	'Section Head Inventory Control',
	'Manager Procurement',
	'Section Head Purchasing',
	'Section Head Bagian',
	'Manager Fungsi',
	'Admin',
]

class Header extends Component {
	state = {
		isActive: false,
		navigations: []
	}

	toggle = () => {  
	  this.setState({isActive: !this.state.isActive}) 
	}

	componentDidMount() {
		const token = localStorage.getItem('token')
		let role = token && jwt.decode(token).user.role
		let navigations = []

		if (role === 1) {
			navigations = [
				{
					name: 'Produk',
					url: '/my-products',
				},
				{
					name: 'Order',
					url: '/list-orders',
				},
				{
					name: 'Ubah Password',
					url: '/change-password',
				}
				
			]
		} else if (role === 2) {
			navigations = [
				{
					name: 'Produk',
					url: '/pic-products',
				},
				{
					name: 'Order',
					url: '/orders',
				},
				{
					name: 'Ubah Password',
					url: '/change-password',
				}
			]
		} else if (role === 3) {
			navigations = [
				{
					name: 'Order',
					url: '/orders',
				},
				{
					name: 'Ubah Password',
					url: '/change-password',
				}
			]
		} else if (role === 9) {
			navigations = [
				{
					name: 'Manajemen',
					url: '/users',
					child:[
						{
							name: 'Master Fungsi',
							url: '/fungsi',
						},
						{
							name: 'Master Bagian',
							url: '/bagian',
						},
						{
							name: 'Activity Log',
							url: '/activity-log',
						},
						{
							name: 'Users',
							url: '/users',
						},
						{
							name: 'Vendors',
							url: '/vendors',
						}
					]
				},
				{
					name: 'Order',
					url: '/orders',
				},
				{
					name: 'Ubah Password',
					url: '/change-password',
				},
				{
					name: 'Product',
					url: '/list-product',
				}
			]
		}
		else if (role > 3) {
			navigations = [
				{
					name: 'Order',
					url: '/orders',
				},
				{
					name: 'Ubah Password',
					url: '/change-password',
				}
			]
		}

		this.setState({navigations});

	}

	menuChild = (item) => {
		return (
			<div className="dropdown">
			  <span className="labelMenu">{item.name}</span>
			  <div className="dropdown-content">
			  	{
						item.child.map((items, index) => (
							<Link 
		  	  			to={items.url} 
		  	  			key={index} 
		  	  			className={`navbar-item ${item.url === '#' ? 'tooltip is-tooltip-right' : ''}`}
		  	  			data-tooltip="Fitur dalam pengembangan"
		  	  		>	
		  	  		  {items.name}
		  	  		</Link>
							)
						)
					}
			  </div>
			</div>
		)
	}

	render() {
		let {isActive, navigations} = this.state

		return (
			<div>
				<nav className="navbar has-shadow" role="navigation" aria-label="main navigation">
				  <div className="container">
				  	<div className="navbar-brand">
				  	  <Link to="/" className="navbar-item">
				  	    <img src="https://upload.wikimedia.org/wikimedia/id/b/b8/Pertamina.png"  height="28" alt="logo" />
				  	    <b style={{margin: 5}}>Direct Buying</b>
				  	  </Link>

				  	  <div className={`navbar-burger ${isActive && 'is-active'}`} onClick={this.toggle}> 
				  	    <span aria-hidden="true"></span>
				  	    <span aria-hidden="true"></span>
				  	    <span aria-hidden="true"></span>
				  	  </div>
				  	</div>

				  	<div className={`navbar-menu ${isActive && 'is-active'}`}>
				  	  <div className="navbar-start">
					  	  <a 
					  	  	href="http://e-logisticruiv.com"
					  	  	className={`navbar-item `}
					  	  >	
					  	    E-Logistic RU IV
					  	  </a>
					  	  
				  	  {
				  	  	navigations.length > 0 &&
				  	  	navigations.map((item, index) => (
				  	  		<div className="boxMenu">
						  	  	{
						  	  		item.child ?
						  	  			this.menuChild(item)
						  	  		:
						  	  		<Link 
						  	  			to={item.url} 
						  	  			key={index} 
						  	  			className={`navbar-item ${item.url === '#' ? 'tooltip is-tooltip-right' : ''}`}
						  	  			data-tooltip="Fitur dalam pengembangan"
						  	  		>	
						  	  		  {item.name}
						  	  		</Link>
						  	  	}
				  	  		</div>
				  	  	))
				  	  }
				  	  </div>

				  	  <div className="navbar-end">
				  	  	{
				  	  		auth.isAuth() &&
				  	  		<div className="navbar-item">
				  	  			<span>{auth.getUser().name} - {roles[auth.getUser().role]}</span>
				  	  		</div>
				  	  	}
				  	  	{
				  	  		authorize.canCreateOrder() &&
				  	  		<div className="navbar-item">
				  	  			<Link to="/cart" className="button is-white">
				  	  		    <span className="icon is-small has-text-link">
				  	  		      <i className="fas fa-shopping-cart"></i>
				  	  		      {
				  	  		      	this.props.cart.length > 0 &&
				  	  		      	<span className="notifLabel" style={{marginTop: -20, fontSize: 11}}>
				  	  		      		{this.props.cart.length}
				  	  		      	</span>
				  	  		      }
				  	  		    </span>
				  	  		  </Link>
				  	  		</div>
				  	  	}

				  	    <div className="navbar-item">
				  	      <div className="buttons">
				  	        {
				  	        	auth.isAuth() ?
					  	        	<button 
					  	        		onClick={() => auth.logout()}
					  	        		className="button is-outlined is-rounded"
					  	        	>
					  	        	  Logout
					  	        	</button>
				  	        	:
					  	        	<Link 
					  	        		to="/login" 
					  	        		className="button is-primary is-outlined is-rounded"
					  	        	>
					  	        	  <strong>Log in</strong>
					  	        	</Link>
				  	        }
				  	      </div>
				  	    </div>
				  	  </div>
				  	</div>
				  </div>
				</nav>
			</div>
		);
	}
}

const mapStateToProps = ({cart}) => {
  return {cart}
}

export default withRouter(connect(mapStateToProps)(Header))
