import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import numeral from 'numeral'
import { connect } from 'react-redux'

import { BASE_URL } from '../../constants'
import auth from '../../services/auth'
import authorize from '../../services/authorize'
import * as actions from '../../redux/actions'

class ProductCardVerify extends Component {
	renderActionButton = (item) => {
		switch(auth.getRole()){
			case 1:
				if (authorize.belongs(item.vendor && item.vendor.companyName, 'companyName')) {
					return (
						<div className="has-text-right">
							<Link to={`/my-products/${item._id}/edit`} className="button is-small is-outlined is-rounded" style={{marginRight: 3}}>Edit</Link>
						</div>
					)
				}
				return
			default:
				if (authorize.canCreateOrder()) {
					return (
						<div className="has-text-right">
						<Link to={`/products-veryfy/${item._id}`} className="button is-small is-success is-rounded" style={{marginRight: 3}}>Detail</Link>
						</div>
					)
				}

				return
		}
	}
	render() {
		const {item} = this.props

		return (
			<div className="card">
				<div className="card-image">
					<Link to={`/products-veryfy/${item._id}`}>
						<figure className="image is-square">	
							{
								item.featuredImage !== undefined && item.featuredImage !== null ?
									<img src={`${BASE_URL}/img/${item.featuredImage}`} alt="product" style={{objectFit: 'contain'}}/>	
								:
		      	  		<img src="/img/default_product.png" alt="product" style={{objectFit: 'cover'}}/>
							}
						</figure>
					</Link>
				   
				</div>
				<div className="card-content">
					<div className="content">
						<Link to={`/products/${item._id}`} style={{fontSize: 14}}>
							<p><b>{item.name}</b></p>
						</Link>
						<b>{numeral(item.price).format('$ 0,0')}</b>
						<br/>
						<div style={{fontSize: 14, marginTop: 10}}>
							<b>Kode:</b>&nbsp;
							{
								item.code || <button className="button is-small is-rounded is-warning tooltip" data-tooltip="Menunggu Verifikasi">--</button>
							}
						</div>
						<p style={{fontSize: 12}}>
							<b>Brand</b>: {item.brand}
							<br/>
							<b>Vendor</b>: {!item.vendor.vendor ?  '' : item.vendor.vendor.name}
							<br/>
							<b>Tipe</b>: {item.stockType === 'nonstock' ? 'Non Stock (N)' : 'Stock (L)'}
						</p>
						{this.renderActionButton(item)}
					</div>
				</div>
			</div>
		);
	}
}


export default connect(null, actions)(ProductCardVerify)
