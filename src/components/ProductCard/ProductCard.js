import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import numeral from 'numeral'
import { connect } from 'react-redux'
import Swal from 'sweetalert2/dist/sweetalert2.js'

import { BASE_URL } from '../../constants'
import apiCall from '../../services/apiCall'
import auth from '../../services/auth'
import authorize from '../../services/authorize'
import * as actions from '../../redux/actions'
import 'sweetalert2/src/sweetalert2.scss'

class ProductCard extends Component {

	onDelete = (id) => {
		Swal.fire({
		  title: 'Anda Yakin?',
		  text: 'Hapus data produk ?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Ya, hapus!',
		  cancelButtonText: 'Tidak'
		}).then((result) => {
		  if (result.value) {
		    this.deleteProduct(id)
		  } 
		})
	}

	deleteProduct = (id) => {
		apiCall.delete(`/products/${id}`)
		.then(res => {
			Swal.fire('Delete Success', '', 'success')
			window.location = ''
		})
		.catch(err => {
			if (err.response) {
				Swal.fire(err.message, '', 'error')
			} else {
				Swal.fire('Terjadi kesalahan pada sistem', '', 'error')
			}
		})
	}

	renderActionButton = (item) => {
		switch(auth.getRole()){
			case 1:
				if (authorize.belongs(item.vendor && item.vendor.companyName, 'companyName')) {
					return (
						<div className="has-text-right">
							<Link to={`/my-products/${item._id}/edit`} className="button is-small is-outlined is-rounded is-success" style={{marginRight: 3}}>Edit</Link>
							<button onClick={() => this.onDelete(item._id)} className="button is-small is-outlined is-rounded is-danger" style={{marginRight: 3}}>Drop</button>
						</div>
					)
				}
				return
			default:
				if (authorize.canCreateOrder()) {
					return (
						<div className="has-text-right">
							<button 
								className="button is-small is-primary is-rounded"
								style={{marginRight: 3}}
								onClick={() => this.props.addToCart(item)}
							>
								Add to Cart
							</button>
						</div>
					)
				}

				return
		}
	}
	render() {
		const {item} = this.props
		// console.log(item, 'item ==============')
		return (
			<div className="card">
				<div className="card-image">
					<Link to={`/products/${item._id}`}>
						<figure className="image is-square">	
							{
								item.featuredImage !== undefined && item.featuredImage !== null ?
									<img src={`${BASE_URL}/img/${item.featuredImage}`} alt="product" style={{objectFit: 'contain'}}/>	
								:
		      	  		<img src="/img/default_product.png" alt="product" style={{objectFit: 'cover'}}/>
							}
						</figure>
					</Link>
				   
				</div>
				<div className="card-content">
					<div className="content">
						<Link to={`/products/${item._id}`} style={{fontSize: 14}}>
							<p><b>{item.name}</b></p>
						</Link>
						<b>{numeral(item.price).format('$ 0,0')}</b>
						<br/>
						<div style={{fontSize: 14, marginTop: 10}}>
							<b>Kode:</b>&nbsp;
							{
								item.code || <button className="button is-small is-rounded is-warning tooltip" data-tooltip="Menunggu Verifikasi">--</button>
							}
						</div>
						<p style={{fontSize: 12}}>
							<b>Brand</b>: {item.brand}
							<br/>
							<b>Vendor</b>: {!item.vendor.vendor ?  '' : item.vendor.vendor.name}
							<br/>
							<b>Tipe</b>: {item.stockType === 'nonstock' ? 'Non Stock (N)' : 'Stock (L)'}
						</p>
						{this.renderActionButton(item)}
					</div>
				</div>
			</div>
		);
	}
}


export default connect(null, actions)(ProductCard)
