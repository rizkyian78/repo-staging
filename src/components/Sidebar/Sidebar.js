import React, { Component } from 'react';
import {Link} from 'react-router-dom'

class Sidebar extends Component {
	render() {
		return (
			<aside className="menu">
			  <ul className="menu-list">
			    <li ><Link to="#" className="is-active">Inbox</Link></li>
			    <li><Link to="#">Selesai</Link></li>
			  </ul>
			</aside>
		);
	}
}

export default Sidebar
