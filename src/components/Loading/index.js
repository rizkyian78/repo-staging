import React, { Component } from 'react';
import { FadeLoader } from 'react-spinners';

const override = {
    display: 'block',
    margin: 0,
    borderColor: 'red'
}

class Loading extends Component {

	render() {
		const loading = this.props.load
    return (
    	<div class={loading ? 'modal is-active' : 'modal'}>
			  <div class="modal-background"></div>
			  <div>
		        <FadeLoader
		          css={override}
		          sizeUnit={"px"}
		          size={150}
		          color={'gray'}
		          loading={loading}
		        />
		        <label style={{color: '#000'}}>Loading...</label>
			  </div>
			</div>
    )
  }
}

export default Loading