import React, { Component } from 'react';

import './App.css';

class App extends Component {
  render() {
    return (
      <section className="section">
        <div className="container">
          App
        </div>
      </section>
    );
  }
}

export default App;
