import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import numeral from 'numeral'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// Redux
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor} from './redux/configureStore'

import './index.css';
import './App.css';
import * as serviceWorker from './serviceWorker';

import Header from './components/Header/Header'
import Home from './pages/Home/Home'
import Search from './pages/Search/Search'
import Order from './pages/Order/Order'
import OrderDetail from './pages/Order/OrderDetail'
import OrderVendor from './pages/Order/OrderVendor'
import Login from './pages/Login/Login'
import MyProduct from './pages/MyProduct/MyProduct'
import MyProductCreate from './pages/MyProduct/MyProductCreate'
import MyProductEdit from './pages/MyProduct/MyProductEdit'
import ProductDetail from './pages/ProductDetail/ProductDetail'
import ProductDetailVerify from './pages/ProductDetail/ProductDetailVerify'
import PicProduct from './pages/PicAnalyst/PicProduct'
import Cart from './pages/Cart/Cart'
import UserList from './pages/User/UserList'
import VendorList from './pages/User/VendorList'
import UserCreate from './pages/User/UserCreate'
import UserEdit from './pages/User/UserEdit'
import VendorEdit from './pages/User/VendorEdit'
import ChangePassword from './pages/User/ChangePassword'
import ListProduct from './pages/Product/ListProduct'
import UpdateProductVendor from './pages/Product/UpdateProductVendor'
import Fungsi from './pages/Master/Fungsi'
import Bagian from './pages/Master/Bagian'
import ActivityLog from './pages/ActivityLog'

class Root extends Component {
	constructor() {
	  super()
	  numeral.register('locale', 'id', {
	    delimiters: {
	      thousands: '.',
	      decimal: ','
	    },
	    abbreviations: {
	      thousand: 'Ribu',
	      million: 'Juta',
	      billion: 'Milliar',
	      trillion: 'Trilliun'
	    },
	    ordinal: function (number) {
	        return number === 1 ? 'er' : 'ème';
	    },
	    currency: {
	      symbol: 'Rp'
	    }
	  })

	  numeral.locale('id')
	}

	render() {
		return(
			<Provider store={store}>
				<PersistGate loading={'Loading...'} persistor={persistor}>
					<Router>
						<div>
							<Header />
							<Switch>
								<Route path="/login" component={Login} />
								<Route path="/search" component={Search} />
								<Route path="/cart" component={Cart} />
								
								<Route path="/users/:id/edit" component={UserEdit} />
								<Route path="/users/create" component={UserCreate} />
								<Route path="/users" component={UserList} />
								<Route path="/vendors" component={VendorList} />
								<Route path="/change-password" component={ChangePassword} />
								
								<Route path="/vendors/:id/edit" component={VendorEdit} />

								<Route path="/products/:id" component={ProductDetail} />
								<Route path="/products-veryfy/:id" component={ProductDetailVerify} />
								<Route path="/update-product-vendor/:id" component={UpdateProductVendor} />

								<Route path="/my-products/:id/edit" component={MyProductEdit} />
								<Route path="/my-products/create" component={MyProductCreate} />
								<Route path="/my-products" component={MyProduct} />
								<Route path="/list-product" component={ListProduct} />
								<Route path="/pic-products" component={PicProduct} />

								<Route path="/orders/:id" component={OrderDetail} />
								<Route path="/orders" component={Order} />
								<Route path="/list-orders" component={OrderVendor} />
								
								<Route path="/fungsi" component={Fungsi} />
								<Route path="/bagian" component={Bagian} />

								<Route path="/activity-log" component={ActivityLog} />
								<Route path="/" component={Home} />
							</Switch>
						</div>
					</Router>
				</PersistGate>
			</Provider>
		)
	}
}

ReactDOM.render(
	<Root />,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
