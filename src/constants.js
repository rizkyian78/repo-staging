const url = {
  production: 'https://api.directbuyingruiv.com',
  staging: 'https://api-directbuying.nusantech.com',
  development: 'http://localhost:5000'
}

const ENV = process.env.REACT_APP_BUILD_ENV || 'development'

export const BASE_URL = url[ENV]

