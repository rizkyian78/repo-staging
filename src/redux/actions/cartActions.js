import { 
	ADD_TO_CART, 
	CHANGE_QUANTITY,
	EMPTY_CART,
	DELETE_ITEM
} from './types'

export const addToCart = (product) => {
	return {
		type: ADD_TO_CART,
		payload: product
	}
}

export const changeQuantity = (item, qty) => {
	return {
		type: CHANGE_QUANTITY,
		payload: {item: item, quantity: qty}
	}
}

export const emptyCart = () => {
	return {
		type: EMPTY_CART,
	}
}

export const deleteItem = (product) => {
	return {
		type: DELETE_ITEM,
		payload: product
	}
}