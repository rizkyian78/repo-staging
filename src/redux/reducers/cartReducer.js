import {
	ADD_TO_CART,
	CHANGE_QUANTITY,
	EMPTY_CART,
	DELETE_ITEM
} from '../actions/types'
import swal from 'sweetalert2'

const INITIAL_STATE = []

export default function(state = INITIAL_STATE, action) {
	switch(action.type) {
		case ADD_TO_CART:
			let temp = {
				product: action.payload,
				quantity: 1
			}

			if (state.length > 0) {
				// cek sama2 stockType?
				if (action.payload.stockType !== state[0].product.stockType) {
					swal.fire('Tipe Stock harus sejenis', 'Hapus dulu produk di Cart bila ingin memilih tipe stock berbeda', 'error')
					return state
				}
				// cek 
				else if(action.payload.vendor.companyName !== state[0].product.vendor.companyName) {
					swal.fire('Vendor harus sama', 'Hapus dulu produk di Cart bila ingin memilih vendor berbeda', 'error')
					return state
				}
				else {
					let index = state.findIndex(item => item.product._id === action.payload._id)
					if (index !== -1) {
						swal.fire({
						  title: 'Berhasil menambah quantity produk di keranjang',
						  text: '',
						  type: 'success',
						  showCancelButton: true,
						  confirmButtonText: 'Produk Lain',
						  cancelButtonText: 'Ke Keranjang'
						}).then((result) => {
						  if (result.value) {
						    console.log('')
						  } else {
						  	window.location = '/cart'
						  }
						})
						return updateObjectInArray(state, action)
					}

					swal.fire({
					  title: 'Berhasil menambah produk ke keranjang',
					  text: '',
					  type: 'success',
					  showCancelButton: true,
					  confirmButtonText: 'Produk Lain',
					  cancelButtonText: 'Ke Keranjang'
					}).then((result) => {
					  if (result.value) {
					    console.log('')
					  } else {
					  	window.location = '/cart'
					  }
					})
				  return [...state, temp]
				}				
			}
			swal.fire({
			  title: 'Berhasil menambah produk ke keranjang',
			  text: '',
			  type: 'success',
			  showCancelButton: true,
			  confirmButtonText: 'Produk Lain',
			  cancelButtonText: 'Ke Keranjang'
			}).then((result) => {
			  if (result.value) {
			    console.log('')
			  } else {
			  	window.location = '/cart'
			  }
			})
			return [...state, temp]
		case CHANGE_QUANTITY:
			return changeQuantity(state, action)
		case EMPTY_CART:
			return INITIAL_STATE
		case DELETE_ITEM:
			return state.filter(item => item.product._id !== action.payload._id)
		default:
			return state
	}
}


function updateObjectInArray(array, action) {
	return array.map(item => {
    if (item.product._id !== action.payload._id) {
      // kalau ga ketemu, tambahin item nya
      return item
    }

    // kalau ketemu, item nya diubah
    let newItem = item
    newItem.quantity +=1

    return {...item, ...newItem}
  })
}

function changeQuantity(array, action) {
	return array.map(item => {
    if (item.product._id !== action.payload.item._id) {
      // kalau ga ketemu, tambahin item nya
      return item
    }

    // kalau ketemu, item nya diubah
    let newItem = item
    newItem.quantity = action.payload.quantity

    return {...item, ...newItem}
  })
}

